<%@page import="service.WareService"%>
<%@page import="entity.Classs"%>
<%@page import="service.ClasssService"%>

<%@page import="entity.User"%>
<%@page import="entity.Ware"%>
<%@page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>星星花店</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />

</head>
	<%
		//总价
		double s;
		//购物车长度
		int cartlistsize;
		if (request.getSession().getAttribute("s") == null) {
			s = 0;
		} else {
			s = (double) request.getSession().getAttribute("s");
		}

		if (request.getSession().getAttribute("cartlistsize") == null) {
			cartlistsize = 0;
		} else {
			cartlistsize = (int) request.getSession().getAttribute("cartlistsize");
		}
		WareService wareservice=new WareService();
		List<Ware> warelist=wareservice.all();
		ClasssService classsService=new ClasssService();
		List<Classs> classsList=classsService.findAll();
	%>
<body>
<div id="wrap">

       <div class="header">
       		<div class="logo"><a href="index.jsp"><img src="images/logo.gif" alt="" title="" border="0" /></a></div>            
        <div id="menu">
            <ul>                                                                       
            <li class="selected"><a href="index.jsp">首页</a></li>
            
            <li><a href="${pageContext.request.contextPath }/AllSelect">花卉</a></li>
            <li><a href="${pageContext.request.contextPath }/AllBigServlet">特价礼品</a></li>
            
            <li>
				<%
				User user = (User)request.getSession().getAttribute("user"); 
				if(user!=null){ %>
				<a href="JumpIntoQuaryinfo">${name}</a>
				<a href="${pageContext.request.contextPath}/Logout" class="b">登出</a>
				<%} else{%>
				<a href="JumpIntoLoginServlet">登录</a>
				<a href="JumpIntoRegist" >注册</a>&nbsp;
				<%} %>
				
			</li> 
			<li><a href="JumpIntoOrder">个人订单</a></li>
            
            <li><SCRIPT>  
			   var d = new Date();   
			  document.write(d.getFullYear() + "年" +(d.getMonth() + 1) + "月" + d.getDate() + "日");   
			  document.write(' 星期'+'日一二三四五六'.charAt(new Date().getDay()));   
			 </SCRIPT></li>
            </ul>
        </div>     
            
            
       </div> 
       
       
       <div class="center_content">
       	<div class="left_content">
        	
            <div class="title"><span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>热卖产品</div>
        
        <% for(int i=0;i<2;i++){%>
       
        	<div class="feat_prod_box">
					<div class="prod_img"><a href="${pageContext.request.contextPath }/WareServlet?id=<%= warelist.get(i).getId() %>">
					<img src="${pageContext.request.contextPath}<%="/photo/"+warelist.get(i).getPicture1() %>" alt="" title="" border="0" /></a></div>                
                <div class="prod_det_box">
                	<div class="box_top"></div>
                    <div class="box_center">
                    <div class="prod_title"><%= warelist.get(i).getName() %></div>
                    <p class="details"><%=warelist.get(i).getInfo() %></p>
					<a href="${pageContext.request.contextPath }/WareServlet?id=<%= warelist.get(i).getId() %>" class="more">- more details -</a>
                    <div class="clear"></div>
                    </div>
                    
                    <div class="box_bottom"></div>
                </div>    
            <div class="clear"></div>
            </div>	
          <% }%>  
                  
            
           <div class="title"><span class="title_icon"><img src="images/bullet2.gif" alt="" title="" /></span>最新产品</div> 
           
           <div class="new_products">
           		<% for(int i=warelist.size()-1;i>=warelist.size()-6;i--){%>
       
                    <div class="new_prod_box">
                        <a href="${pageContext.request.contextPath }/WareServlet?id=<%= warelist.get(i).getId() %>"><%= warelist.get(i).getName() %></a>
                        <div class="new_prod_bg">
                        <span class="new_icon"><img src="images/new_icon.gif" alt="" title="" /></span>
						<a href="${pageContext.request.contextPath }/WareServlet?id=<%= warelist.get(i).getId() %>"><img src="${pageContext.request.contextPath }<%="/photo/"+warelist.get(i).getPicture1() %>" alt="" title="" class="thumb" border="0" /></a>
                        </div>           
                    </div>
                   
                  <% }%>  
            </div> 
          
            
        <div class="clear"></div>
        </div>
		<!--end of left content-->
        
        <div class="right_content">
                
                
              <div class="cart">
                  <div class="title"><span class="title_icon"><img src="images/cart.gif" alt="" title="" /></span>我的购物车</div>
                  <div class="home_cart_content">
                  <%=cartlistsize %> 件产品 | <span class="red">总价： ￥<%=s %></span>
                  </div>
                  <a href="JumpIntoCart" class="view_cart">查看</a>
              
              </div>
                       
            	
        
        
             <div class="title"><span class="title_icon"><img src="images/bullet3.gif" alt="" title="" /></span>关于星星花店</div> 
             <div class="about">
             <p>
             <img src="images/about.gif" alt="" title="" class="right" /> 
				星星花店全称星星花卉网上商城，是中国鲜花行业品牌服务商（曾多次次获得行业殊荣：中国电子商务协会评定为“中国互联网电子商务鲜花礼品行业龙头企业”），隶属于鲜花商城有限公司，网站自创办以来一直保持高速成长，连续四年增长率均超过100%，目前规模和影响力均位居中国鲜花礼品网站前列，销量已连续3年在同类网站中处于领先地位。  
             </p>
             
             </div>
             
             <div class="right_box">        
             	<div class="title"><span class="title_icon"><img src="images/bullet4.gif" alt="" title="" /></span>促销产品</div> 
                    <div class="new_prod_box">
                        <a href="${pageContext.request.contextPath }/WareServlet?id=<%= warelist.get(2).getId() %>">
                        	<%=warelist.get(2).getName() %></a>
                        <div class="new_prod_bg">
                        <a href="${pageContext.request.contextPath }/WareServlet?id=<%= warelist.get(2).getId() %>"><img src="${pageContext.request.contextPath }<%="/photo/"+warelist.get(2).getPicture1() %>" alt="" title="" class="thumb" border="0" /></a>
                        
                        </div>           
                        
                    </div>
                    
                    <div class="new_prod_box">
                        <a href="${pageContext.request.contextPath }/WareServlet?id=<%= warelist.get(3).getId() %>">
                        	<%=warelist.get(3).getName() %></a>
                        <div class="new_prod_bg">
                        <a href="${pageContext.request.contextPath }/WareServlet?id=<%= warelist.get(3).getId() %>"><img src="${pageContext.request.contextPath }<%="/photo/"+warelist.get(3).getPicture1() %>" alt="" title="" class="thumb" border="0" /></a>
                        
                        </div>           
                        
                    </div>               
           
             
             </div>
             
             <div class="right_box">
             
             	<div class="title"><span class="title_icon"><img src="images/bullet5.gif" alt="" title="" /></span>产品分类</div> 
                
                <ul class="list">
                <%for(int i=0;i<classsList.size();i++){ %>
                	<li><a href="${pageContext.request.contextPath }/WareSelect?classs=<%= classsList.get(i).getClasss_name()%> "><%= classsList.get(i).getClasss_name()%></a></li>
                <%} %>                                      
                </ul>
                
 
             
             </div>         
             
        
        </div><!--end of right content-->
        
        
       
       
       <div class="clear"></div>
       </div><!--end of center content-->
       
        
       <div class="footer">
         
           <p>&copy;星星花店</p>
       </div>
    	

</div>
</body>
</html>