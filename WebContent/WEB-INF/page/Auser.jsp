﻿<%@page import="entity.User"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>用户管理</title>
<style type="text/css">
	#rounded-corner thead th.rounded-company
	{
	width:26px;
	background: #60c8f2 url('images/left.jpg') left top no-repeat;
	}
	#rounded-corner thead th.rounded-q4
	{
	background: #60c8f2 url('images/right.jpg') right top no-repeat;
	}
	#rounded-corner th
	{
	padding: 8px;
	font-weight: normal;
	font-size: 13px;
	color: #039;
	background: #60c8f2;
	}
	#rounded-corner td
	{
	padding: 8px;
	background: #ecf8fd;
	border-top: 1px solid #fff;
	color: #669;
	}
	#rounded-corner tbody tr:hover td
	{
	background: #d2e7f0;
	}
	h2{
	font-size:30px;
	color:#256c89;
	font-weight:300px;
	padding: 8px;
	margin:0px;
	clear:both;
	}
	h1{
	font-size:36px;
	color:#256c89;
	font-weight:300px;
	padding: 8px;
	margin:0px;
	clear:both;
	}
	h3{
	font-size:16px;
	color:#256c89;
	font-weight:300px;
	padding: 8px;
	margin:0px;
	clear:both;
	}
	a{
	font-size:16px;
	color:#256c89;
	font-weight:300px;
	padding: 0px;
	margin:0px;
	clear:both;
	}
	body{
     background-image:  url("images/bg.jpg");
    }
     #particles-js {
        position: absolute;
        top: 0;
        width: 100%;
    }
</style>
</head>
<body>
	<% ArrayList<User> alluser=(ArrayList<User>)request.getSession().getAttribute("alluser"); %>
	<table width="1000px" border="0" style="text-align: center;" align="center"id="rounded-corner">
		<caption><h2 style="padding-bottom: 30px;">用户管理</h2></caption>
		<tr>
			<th>id</th>
			<th>用户名</th>
			<th>年龄</th>
			<th>家庭住址</th>
			<th>电话</th>
			<th>生日</th>
			<th>邮箱</th>
			<th width="160px">操作</th>
		</tr>
		<%for(int i=0;i<alluser.size();i++){ %>
			<tr>
				<td><%=alluser.get(i).getId() %></td>
				<td><%=alluser.get(i).getUsername() %></td>
				<td><%=alluser.get(i).getAge() %></td>
				<td><%=alluser.get(i).getAddress() %></td>
				<td><%=alluser.get(i).getPhone() %></td>
				<td><%=alluser.get(i).getInfo_birthday() %></td>
				<td><%=alluser.get(i).getInfo_mail() %></td>
				<td><a href="${pageContext.request.contextPath}/Apowerup?id=<%=alluser.get(i).getId() %>" >提升权限</a>&nbsp;&nbsp;
					<a href="${pageContext.request.contextPath}/Adeleteuser?id=<%=alluser.get(i).getId() %>">删除用户</a>
				</td>
			</tr>
		<%} %>
	</table>
	<a href="JumpIntoAmain"><h3>返回主页</h3></a>
<!-- particles.js container -->
<div id="particles-js" style="z-index: -2"></div>
<!-- scripts -->
<script src="js/particles.min.js"></script>
<script src="js/app.js"></script>
<!-- stats.js -->
</body>
</html>