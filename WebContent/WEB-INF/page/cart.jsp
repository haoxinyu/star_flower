<%@page import="service.ClasssService"%>
<%@page import="entity.Classs"%>
<%@page import="entity.User"%>
<%@page import="entity.Cart"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>星星花店</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />

</head>

<body>
	<%
		request.setCharacterEncoding("UTF-8");
		//所有购物车的商品
		ArrayList<Cart> u;
		if (request.getSession().getAttribute("cartlist") != null) {
			u = (ArrayList<Cart>) request.getSession().getAttribute("cartlist");
		} else {
			u = null;
		}
		//总价
		double s;
		//购物车长度
		int cartlistsize;
		if (request.getSession().getAttribute("s") == null) {
			s = 0;
		} else {
			s = (double) request.getSession().getAttribute("s");
		}

		if (request.getSession().getAttribute("cartlistsize") == null) {
			cartlistsize = 0;
		} else {
			cartlistsize = (int) request.getSession().getAttribute("cartlistsize");
		}
		int temp=1;
		request.getSession().setAttribute("temp", temp);
		List<Classs>classsList=(List<Classs>)request.getAttribute("classsList");
	%>

	<script type="text/javascript">
		var request = false;
		function findselectcart() {

			if (window.XMLHttpRequest) {//判断版本，这是新版本
				request = new XMLHttpRequest();
			}
			if (window.ActiveXObject) {//早期版本浏览器
				request = new ActiveXObject();
			}

			if (request) {
				var url = "${pageContext.request.contextPath}/FindSelectCartServlet";//servlet路径
				request.open("post", url, true);//设置方法并提交
				request.setRequestHeader("Content-Type",
						"application/x-www-form-urlencoded");
				request.onreadystatechange = test;

				var list ="";
				var checkcart = document.getElementsByName("checkcart");
				for (var i = 0; i < checkcart.length; i++) {
					if (checkcart[i].checked == true) {
						list = list + checkcart[i].value + ",";
					}
				}
				request.send("list=" + list);
			}
		}
		function test() {

			if (request.readyState == 4) {//准备状态

				if (request.status == 200) {
					//成功了就

					var result = request.responseText;
					var strs = new Array();
					strs = result.split(",");
					document.getElementById("sp").innerHTML = strs[0];
					document.getElementById("sn").innerHTML = strs[1];
				}
			}
		}
	</script>


	<div id="wrap">
		<div class="header">
			<div class="logo">
				<a href="index.jsp"><img src="images/logo.gif" alt="" title=""
					border="0" /></a>
			</div>
			<div id="menu">
				<ul>
					<li ><a href="index.jsp">首页</a></li>

					<li><a href="${pageContext.request.contextPath }/AllSelect">花卉</a></li>
					<li><a href="${pageContext.request.contextPath }/AllBigServlet">特价礼品</a></li>
				<li>
				<%
				User user = (User)request.getSession().getAttribute("user"); 
				if(user!=null){ %>
				<a href="JumpIntoQuaryinfo">${name}</a>
				<a href="${pageContext.request.contextPath}/Logout" class="b">登出</a>
				<%} else{%>
				<a href="JumpIntoLoginServlet">登录</a>
				<a href="JumpIntoRegist" >注册</a>&nbsp;
				<%} %>
				
			</li> 
			<li><a href="JumpIntoOrder">个人订单</a></li>
					
					
					
					<li><SCRIPT>
						var d = new Date();
						document.write(d.getFullYear() + "年"
								+ (d.getMonth() + 1) + "月" + d.getDate() + "日");
						document.write(' 星期'
								+ '日一二三四五六'.charAt(new Date().getDay()));
					</SCRIPT></li>
				</ul>
			</div>


		</div>


		<div class="center_content">
			<div class="left_content">
				<div class="crumb_nav">
					<a href="index.jsp">首页</a> &gt;&gt; <a href="${pageContext.request.contextPath }/AllSelect">产品列表</a>
					 &gt;&gt; 购物车
				</div>
				<div class="title">
					<span class="title_icon"><img src="images/bullet1.gif"
						alt="" title="" /></span>我的购物车
				</div>

				<div class="feat_prod_box_details">

					<table class="cart_table">
						<tr class="cart_title">
							<td>产品列表</td>
							<td>产品名称</td>
							<td>单价</td>
							<td>数量</td>
							<td>总价</td>
							<td>删除</td>
						</tr>

						<%
						
							int k = 0;
							for (int i = 0; i < cartlistsize; i++) {
								if (u.get(i).getType() == 0) {
									k++;
						%>
						<tr>
							<td><a href="${pageContext.request.contextPath }/WareServlet?id=<%=u.get(i).getWareid()%>"><img
									src="images/cart_thumb.gif" alt="" title="" border="0"
									class="cart_thumb" /></a> <input type="checkbox" name="checkcart"
								onclick="findselectcart()" value="<%=i%>"></td>
							<td><%=u.get(i).getWarename()%></td>
							<td><%=u.get(i).getWareprice()%></td>
							<td><%=u.get(i).getWarenum()%></td>
							<td><%=u.get(i).getWarenum() * u.get(i).getWareprice()%></td>
							<td><a
								href="${pageContext.request.contextPath}
					/DeleteCartServlet
					?serialnum=<%=u.get(i).getSerialnum()%>
					&wareid=<%=u.get(i).getUserid() %>
					&userid=<%=u.get(i).getUserid() %>">删除</a></td>
						</tr>
						<%
							}
							}
							cartlistsize = k;
						%>



						<tr>
							<td colspan="1" class="cart_total"><span class="red">&nbsp;选中商品数量:</span></td>
							<td><span id="sn"></span></td>
							<td colspan="2" class="cart_total"><span class="red">商品总数:</span></td>
							<td><%=cartlistsize%></td>
						</tr>

						<tr>
							<td colspan="1" class="cart_total"><span class="red">选中商品总价:</span></td>
							<td>￥<span id="sp"></span></td>
							<td colspan="2" class="cart_total"><span class="red">总价:</span></td>
							<td>￥<%=s%></td>
						</tr>

					</table>
					<a class="continue" href="${pageContext.request.contextPath }/AllSelect">&lt; 继续购物</a> 
					<a class="checkout" href="${pageContext.request.contextPath }/CheckoutServlet">结算 &gt;</a>




				</div>


				<div class="clear"></div>
			</div>
			<!--end of left content-->

			<div class="right_content">


				<div class="cart">
					<div class="title">
						<span class="title_icon"><img src="images/cart.gif" alt=""
							title="" /></span>我的购物车
					</div>
					<div class="home_cart_content">
						<%=cartlistsize%>件产品 | <span class="red">总价：<%=s%></span>
					</div>
					<a href="JumpIntoCart" class="view_cart">查看</a>

				</div>




				<div class="title">
					<span class="title_icon"><img src="images/bullet3.gif"
						alt="" title="" /></span>关于星星花店
				</div>
				<div class="about">
					<p>
						<img src="images/about.gif" alt="" title="" class="right" />
						星星花店全称星星花卉网上商城，是中国鲜花行业品牌服务商（曾多次次获得行业殊荣：中国电子商务协会评定为“中国互联网电子商务鲜花礼品行业龙头企业”），隶属于鲜花商城有限公司，网站自创办以来一直保持高速成长，连续四年增长率均超过100%，目前规模和影响力均位居中国鲜花礼品网站前列，销量已连续3年在同类网站中处于领先地位。
					</p>

				</div>

				<div class="right_box">

					<div class="title">
						<span class="title_icon"><img src="images/bullet4.gif"
							alt="" title="" /></span>促销产品
					</div>
					<div class="new_prod_box">
						<a href="details.jsp">康乃馨</a>
						<div class="new_prod_bg">
							<span class="new_icon"><img src="images/promo_icon.gif"
								alt="" title="" /></span> <a href="#"><img
								src="images/thumb1.gif" alt="" title="" class="thumb" border="0" /></a>
						</div>
					</div>

					<div class="new_prod_box">
						<a href="details.jsp">水仙花</a>
						<div class="new_prod_bg">
							<span class="new_icon"><img src="images/promo_icon.gif"
								alt="" title="" /></span><a href="#"><img
								src="images/thumb2.gif" alt="" title="" class="thumb" border="0" /></a>
						</div>
					</div>


				</div>

				<div class="right_box">

					<div class="title">
						<span class="title_icon"><img src="images/bullet5.gif"
							alt="" title="" /></span>产品分类
					</div>

			<ul class="list">
                <%for(int i=0;i<classsList.size();i++){ %>
                	<li><a href="${pageContext.request.contextPath }/WareSelect?classs=<%= classsList.get(i).getClasss_name()%> "><%= classsList.get(i).getClasss_name()%></a></li>
                <%} %>                                      
                </ul>



				</div>


			</div>
			<!--end of right content-->




			<div class="clear"></div>
		</div>
		<!--end of center content-->


		<div class="footer">
			<p>&copy;星星花店</p>
		</div>


	</div>

</body>
</html>