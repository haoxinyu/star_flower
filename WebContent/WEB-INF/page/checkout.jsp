<%@page import="entity.Classs"%>
<%@page import="service.ClasssService"%>
<%@page import="entity.User"%>
<%@page import="entity.Cart"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
     <%@ page import="java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>星星花店</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script type="text/javascript">
	var request=false;
	
	function zhifubao(){
		var x1=document.getElementById("zhifubao1");
		x1.style.display="none";
		var y1=document.getElementById("zhifubao2");
		y1.style.display="inline";
		var x1=document.getElementById("weixin1");
		x1.style.display="inline";
		var y1=document.getElementById("weixin2");
		y1.style.display="none";
	}
	
	function weixin(){
		var x1=document.getElementById("zhifubao1");
		x1.style.display="inline";
		var y1=document.getElementById("zhifubao2");
		y1.style.display="none";
		var x1=document.getElementById("weixin1");
		x1.style.display="none";
		var y1=document.getElementById("weixin2");
		y1.style.display="inline";
	}

</script>
</head>
<body>
<%  
	request.setCharacterEncoding("UTF-8");
	double s;int cartlistsize;
	if(request.getSession().getAttribute("s")==null){
		s=0;
	}else {
		s=(double)request.getSession().getAttribute("s");
	}
	if(request.getSession().getAttribute("cartlistsize")==null){
		cartlistsize=0;
	}else{
		cartlistsize=(int)request.getSession().getAttribute("cartlistsize");
	}
	ArrayList<Cart> u;
	if(request.getSession().getAttribute("cartlist")!=null){
		u=(ArrayList<Cart>)request.getSession().getAttribute("cartlist");
	}
	else{
		u=null;
	}
	ClasssService classsService=new ClasssService();
	List<Classs> classsList=classsService.findAll();
	int selectlength=(int)request.getSession().getAttribute("selectlength");
	ArrayList<Integer> selectlist=(ArrayList<Integer>)request.getSession().getAttribute("selectlist");
	double sprice=(double)request.getSession().getAttribute("sprice");
%>

<div id="wrap">

       <div class="header">
       		<div class="logo"><a href="index.jsp"><img src="images/logo.gif" alt="" title="" border="0" /></a></div>            
        <div id="menu">
            <ul>                                                                       
            <li class="selected"><a href="index.jsp">首页</a></li>
            
            <li><a href="${pageContext.request.contextPath }/AllSelect">花卉</a></li>
            <li><a href="${pageContext.request.contextPath }/AllBigServlet">特价礼品</a></li>
            
            
            <li>
				<%
				User user = (User)request.getSession().getAttribute("user"); 
				if(user!=null){ %>
				<a href="JumpIntoQuaryinfo">${name}</a>
				<a href="${pageContext.request.contextPath}/Logout" class="b">登出</a>
				<%} else{%>
				<a href="JumpIntoLoginServlet">登录</a>
				<a href="JumpIntoRegist" >注册</a>&nbsp;
				<%} %>
				
			</li> 
			<li><a href="JumpIntoOrder">个人订单</a></li>
            
            
            <li><SCRIPT>  
   var d = new Date();   
  document.write(d.getFullYear() + "年" +(d.getMonth() + 1) + "月" + d.getDate() + "日");   
  document.write(' 星期'+'日一二三四五六'.charAt(new Date().getDay()));   
 </SCRIPT></li>
            </ul>
        </div>     
            
            
       </div> 
       
       
       <div class="center_content">
       	<div class="left_content">
        	<div class="crumb_nav">
            	<a href="index.jsp">首页</a> &gt;&gt; 资金结算
          	</div>
            <div class="title"><span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>资金结算</div>
            <p></p>
            
            
            <form action="PaySelectServlet">
          		<table border="0" width="580"><!-- 580 -->
				<tr>
					<td><hr>
					我们为您提供网上第三方平台支付（支付宝支付、微信支付）的支付方式。目前网上支付有一定金额的限制，由于各公司政策不同，建议您在申请网上支付功能时向银行咨询相关事宜。<table width="100%" border="1" bordercolor="#DBE5F1" bgcolor="#FFFFFF" style="margin: 0px; padding: 0px; color: rgb(64, 64, 64); font-family: 宋体, Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 24px; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; border-collapse: collapse;">
						<tbody>
							<tr>
								<td bgcolor="#DBE5F1">
								<span>平台名称</span></td>
								<td bgcolor="#DBE5F1">
								<span>支付限额</span></td>
								<td bgcolor="#DBE5F1" >
								<span>支持方式</span></td>
							</tr>
							<tr style="height: 250px;">
								<td>
								<input type="radio" name="bank" id="bank1" value="支付宝" onclick="zhifubao()">
								<label for="bank1"><img src="images/zhifubao.jpg" border="0" hspace="0" vspace="0"  width="159" height="62" ><br>
								</label>
&nbsp;</td>
								<td id="zhifubao1" style="border: 0">									
									<p>
									<span style="border: 0">
									余额宝：每日最高2万元；</span></p>
									<p>
									<span style="border: 0">
									银行卡：单笔/单日最高5000元；</span></p>
									<p>
									<span style="border: 0">
									蚂蚁花呗：每月最高2000元；</span>
						
								</td>
								<td style="display: none; border: 0; vertical-align: middle;" id="zhifubao2">
									<div style="vertical-align: middle; padding-top: 40px;">
										<img alt="" src="images/lizhifu.jpg">&nbsp;&nbsp;&nbsp;<img alt="" src="images/youzhifu.jpg">
									</div>
								</td>
								<td >
								<span>
								借记卡/信用卡/余额<br>
&nbsp;</span></td>
							</tr>
							<tr style="height: 250px;">
								<td>								
								<input type="radio" name="bank" id="bank2" value="微信" onclick="weixin()">
								<label for="bank2"><img src="images/weixin.jpg" border="0" hspace="0" vspace="0"  width="159" height="62" ><br>
								</label>
&nbsp;</td>
								<td id="weixin1" style="border: 0">
									<p>
									<span style="border: 0">
									零钱支付：单笔限额5000元，日累计限额1万元；</span></p>
									<p>
									<span style="border: 0">
									银行卡：单笔最高1万元/单日最高5万元；</span></p>
									<p>

&nbsp;</td>
								<td id="weixin2" style="display: none;text-align: center;  border: 0; ">
									<div style="vertical-align: middle; padding-top: 40px;">
										<img alt="" src="images/haoweixin.jpg">&nbsp;&nbsp;&nbsp;<img alt="" src="images/renweixin.jpg">
									</div>
								</td>
								<td>
								<span>
								龙卡储蓄卡/信用卡<br>
&nbsp;</span></td>
							</tr>
					</table>
					</td>
				</tr>
			</table>
		<div style="margin-left:250px;">
		<input type="submit" value="" style="background-image:url('images/order_now.gif');width: 100px;height: 30px;" border="1px solid #3079ED"  >
		</div>
		</form>
            
        <div class="clear"></div>
        </div>
		<!--end of left content-->
        
        <div class="right_content">
                
                
              <div class="cart">
                  <div class="title"><span class="title_icon"><img src="images/cart.gif" alt="" title="" /></span>我的购物车</div>
                  <div class="home_cart_content">
                  <%=cartlistsize %> 件产品 | <span class="red">总价： ￥<%=s %></span>
                  </div>
                  <a href="JumpIntoCart" class="view_cart">查看</a>
              
              </div>
                       
            	
        
        
             <div class="title"><span class="title_icon"><img src="images/bullet3.gif" alt="" title="" /></span>关于星星花店</div> 
             <div class="about">
             <p>
             <img src="images/about.gif" alt="" title="" class="right" /> 
				星星花店全称星星花卉网上商城，是中国鲜花行业品牌服务商（曾多次次获得行业殊荣：中国电子商务协会评定为“中国互联网电子商务鲜花礼品行业龙头企业”），隶属于鲜花商城有限公司，网站自创办以来一直保持高速成长，连续四年增长率均超过100%，目前规模和影响力均位居中国鲜花礼品网站前列，销量已连续3年在同类网站中处于领先地位。  
             </p>
             
             </div>
             
            	<div class="right_box">

					<div class="title">
						<span class="title_icon"><img src="images/bullet4.gif"
							alt="" title="" /></span>促销产品
					</div>
					<div class="new_prod_box">
						<a href="details.jsp">康乃馨</a>
						<div class="new_prod_bg">
							<span class="new_icon"><img src="images/promo_icon.gif"
								alt="" title="" /></span> <a href="#"><img
								src="images/thumb1.gif" alt="" title="" class="thumb" border="0" /></a>
						</div>
					</div>

					<div class="new_prod_box">
						<a href="details.jsp">水仙花</a>
						<div class="new_prod_bg">
							<span class="new_icon"><img src="images/promo_icon.gif"
								alt="" title="" /></span><a href="#"><img
								src="images/thumb2.gif" alt="" title="" class="thumb" border="0" /></a>
						</div>
					</div>


				</div>
             
             <div class="right_box">
             
             	<div class="title"><span class="title_icon"><img src="images/bullet5.gif" alt="" title="" /></span>产品分类</div> 
                
                <ul class="list">
                <%for(int i=0;i<classsList.size();i++){ %>
                	<li><a href="${pageContext.request.contextPath }/WareSelect?classs=<%= classsList.get(i).getClasss_name()%> "><%= classsList.get(i).getClasss_name()%></a></li>
                <%} %>                                      
                </ul>
                
 
             
             </div>         
             
        
        </div><!--end of right content-->
        
        
       
       
       <div class="clear"></div>
       </div><!--end of center content-->
       
              
       <div class="footer">
           <p>&copy;星星花店</p>
       </div>
    

</div>

</body>
</html>