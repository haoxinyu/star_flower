﻿<%@page language="java"
		 contentType="text/html; charset=utf-8"
    	 pageEncoding="utf-8"
    	 import="service.ClasssService"
%>
<%@page import="java.util.*"%>
<%@page import="entity.Classs"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>分类管理</title>
	<style type="text/css">
	#rounded-corner thead th.rounded-company
	{
	width:26px;
	background: #60c8f2 url('images/left.jpg') left top no-repeat;
	}
	#rounded-corner thead th.rounded-q4
	{
	background: #60c8f2 url('images/right.jpg') right top no-repeat;
	}
	#rounded-corner th
	{
	padding: 8px;
	font-weight: 150px;
	font-size:20px;
	color: #039;
	background: #60c8f2;
	}
	#rounded-corner td
	{
	padding: 8px;
	background: #ecf8fd;
	border-top: 1px solid #fff;
	color: #669;
	font-size:15px;
	}
	#rounded-corner tbody tr:hover td
	{
	background: #d2e7f0;
	}
	h2{
	font-size:30px;
	color:#256c89;
	font-weight:300px;
	padding: 8px;
	margin:0px;
	clear:both;
	}
	h3{
	font-size:16px;
	color:#256c89;
	font-weight:300px;
	padding: 8px;
	margin:0px;
	clear:both;
	}
		body{
     background-image:  url("images/bg.jpg");
    }
     #particles-js {
        position: absolute;
        top: 0;
        width: 100%;
    }
</style>
</head>
<body>
	<h2 align="center">分类管理</h2>
	<br/><br/>
	<form action="${pageContext.request.contextPath}/Anewclasss" method="post" align="center">
		请输入新增分类名称<input type="text" name="classname"/>&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit"/>
	</form>
	<br/><br/><br/>
	<table id="rounded-corner" align="center">
		<tr>
			<th width="45px">编号</th>
			<th width="250px">分类名</th>
			<th >操作</th>	
		</tr>
		<%	ClasssService cs=new ClasssService();
			ArrayList<Classs> cc=(ArrayList<Classs>)cs.select();
		%>
 		<%for(Classs k:cc){ %>
		<tr>
			<td><%=k.getClasss_num() %></td>
			<td><%=k.getClasss_name() %></td>
			<td><a href="${pageContext.request.contextPath}/Adeleteclasss?class=<%=k.getClasss_num() %>">
			<img src="images/trash.png" alt="" title="" border="0"></a></td>	
		</tr>
			<%} %>
	</table>
	<a href="JumpIntoAmain">返回主页</a>
		<!-- particles.js container -->
<div id="particles-js" style="z-index: -2"></div>
<!-- scripts -->
<script src="js/particles.min.js"></script>
<script src="js/app.js"></script>
<!-- stats.js -->
</body>
</html>