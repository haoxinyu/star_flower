﻿<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/calendar.css">
<link rel="stylesheet" type="text/css" href="css/cs-select.css" />
<link rel="stylesheet" type="text/css" href="css/style2.css" />
<link rel="stylesheet" type="text/css" href="css/style3.css" />
<link rel="stylesheet" type="text/css" href="css/cs-skin-rotate.css" /> 
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/strength.js"></script>
<script src="js/classie.js"></script>
 <script src="js/selectFx.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>管理界面</title>
<style type="text/css">
	#rounded-corner thead th.rounded-company
	{
	width:26px;
	background: #60c8f2 url('images/left.jpg') left top no-repeat;
	}
	#rounded-corner thead th.rounded-q4
	{
	background: #60c8f2 url('images/right.jpg') right top no-repeat;
	}
	#rounded-corner th
	{
	padding: 8px;
	font-weight: normal;
	font-size: 13px;
	color: #039;
	background: #60c8f2;
	}
	#rounded-corner td
	{
	padding: 8px;
	background: #ecf8fd;
	border-top: 1px solid #fff;
	color: #669;
	}
	#rounded-corner tbody tr:hover td
	{
	background: #d2e7f0;
	}
	h2{
	font-size:30px;
	color:#256c89;
	font-weight:300px;
	padding: 8px;
	margin:0px;
	clear:both;
	}
	h1{
	font-size:40px;
	color:#256c89;
	font-weight:300px;
	padding: 8px;
	margin:0px;
	clear:both;
	}
	 #particles-js {
        position: absolute;
        top: 0;
        width: 100%;
    }
   
</style>
</head>
<body>
	<div style="text-align: center;" align="center">
			<h1><strong>※※星星花店管理系统※※</strong></h1>
		<br/>
		<br/>
		<a href="${pageContext.request.contextPath}/Achangeinfo"><h2>○◎●用户管理●◎○</h2></a>
		<br/>
		<br/>
		<br/>
		<a href="JumpIntoAware"><h2>○◎●商品管理●◎○</h2></a>
		<br/>
		<br/>
		<br/>
		<a href="JumpIntoAclass"><h2>○◎●分类管理●◎○</h2></a>
		<br/>
		<br/>
		<br/>
		<a href="JumpIntoAcart"><h2>○◎●订单管理●◎○</h2><//a>
		<br/>
		<br/>
		<br/>
		<a href="${pageContext.request.contextPath}/Alogout"><h2>○◎●退出登录●◎○</h2></a>
	</div>
	<!-- particles.js container -->
<div id="particles-js" style="z-index: -2"></div>
<!-- scripts -->
<script src="js/particles.min.js"></script>
<script src="js/app.js"></script>
<!-- stats.js -->
</body>
</html>