﻿<%@page import="entity.Ware"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%request.setCharacterEncoding("utf-8"); %>
<%@page import="service.ClasssService"%>
<%@page import="entity.Classs"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style type="text/css">
	#rounded-corner thead th.rounded-company
	{
	width:26px;
	background: #60c8f2 url('images/left.jpg') left top no-repeat;
	}
	#rounded-corner thead th.rounded-q4
	{
	background: #60c8f2 url('images/right.jpg') right top no-repeat;
	}
	#rounded-corner th
	{
	padding: 8px;
	font-weight: normal;
	font-size: 13px;
	color: #039;
	background: #60c8f2;
	}
	#rounded-corner td
	{
	padding: 8px;
	background: #ecf8fd;
	border-top: 1px solid #fff;
	color: #669;
	}
	#rounded-corner tbody tr:hover td
	{
	background: #d2e7f0;
	}
	h2{
	font-size:30px;
	color:#256c89;
	font-weight:300px;
	padding: 8px;
	margin:0px;
	clear:both;
	}
	h3{
	font-size:16px;
	color:#256c89;
	font-weight:300px;
	padding: 8px;
	margin:0px;
	clear:both;
	}
	body{
     background-image:  url("images/bg.jpg");
    }
     #particles-js {
        position: absolute;
        top: 0;
        width: 100%;
    }
</style>
<title>修改商品</title>
</head>
<body>
<%   Ware ww=(Ware)request.getAttribute("ware");
	ClasssService cs=new ClasssService();
	ArrayList<Classs> cc=(ArrayList<Classs>)cs.select();
%>
	<form action="${pageContext.request.contextPath}/Achangeware" method="post" enctype="multipart/form-data">
		<table align="center" id="rounded-corner">
				<caption style="padding: 15px; font-size: 30px;"><h2>商品信息</h2></caption>
			<tr>
				<th style="padding-bottom: 20px;"class="rounded-company">商品序号</th>
				<td style="padding-bottom: 20px;"><input type="text" readOnly name="id" value="<%=ww.getId() %>"/></td>
			</tr>
			<tr>
				<th style="padding-bottom: 20px;">商品名</th>
				<td style="padding-bottom: 20px;"><input type="text"  name="name" value="<%=ww.getName() %>"/></td>
			</tr>
			<tr>
				<th style="padding-bottom: 20px;">原价</th>
				<td style="padding-bottom: 20px;"><input type="text" name="price" value="<%=ww.getPrice() %>"/></td>
			</tr>
			<tr>
				<th style="padding-bottom: 20px;">数量</th>
				<td style="padding-bottom: 20px;"><input type="text" name="num" value="<%=ww.getNum() %>"/></td>
			</tr>
			<tr>
				<th style="padding-bottom: 20px;">是否促销</th>
				<td style="padding-bottom: 20px;">
					<select style="width: 100%;" name=classb>
						<option value="无折扣">无折扣</option>
						<option value="90%off">九折</option>
						<option value="80%off">八折</option>
						<option value="70%off">七折</option>
						<option value="60%off">六折</option>
						<option value="50%off">五折</option>
					</select>
				</td>
			</tr>
			<tr>
				<th style="padding-bottom: 20px;">商品类型</th>
				<td style="padding-bottom: 20px;">
					<select style="width: 100%;" name="classs">
						<%for(int x=0;x<cc.size();x++){%>
						<option value="<%=cc.get(x).getClasss_name()%>"><%=cc.get(x).getClasss_name()%></option>
						<%} %>
					</select>
				</td>
			</tr>
			
			<tr>
				<th style="padding-bottom: 20px;">详细信息</th>
				<td style="padding-bottom: 20px;"><textarea style="width: 100%; height: 300px;" name="info"><%=ww.getInfo() %></textarea></td>
			</tr>
			<tr>
				<th style="padding-bottom: 20px;">图片1</th>
				<td style="padding-bottom: 20px;"><input type="file" name=photo1/></td>
			</tr>
			<tr>
				<th style="padding-bottom: 20px;">图片2</th>
				<td style="padding-bottom: 20px;"><input type="file" name=photo2/></td>
			</tr>
			<tr>
				<th style="padding-bottom: 20px;">图片3</th>
				<td><input type="file" name=photo3 /></td>
			</tr>
			<tr>
				<td align="center" colspan="2"><input  style="width:300px;height:45px;" type="submit" value="提交修改"/></td>				
			</tr>
		</table>	
	</form>
		<a href="JumpIntoAware"><h3>返回</h3></a>
		<!-- particles.js container -->
<div id="particles-js" style="z-index: -2"></div>
<!-- scripts -->
<script src="js/particles.min.js"></script>
<script src="js/app.js"></script>
<!-- stats.js -->
</body>
</html>