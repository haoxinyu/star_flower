<%@page import="entity.Classs"%>
<%@page import="entity.Ware"%>
<%@page import="entity.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
     <%@ page import="java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>星星花店</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />

</head>
<body>
<%  
	//购物车内的总价格
	double s;
	//购物车内商品数量
	int cartlistsize;
 	User user=(User)request.getSession().getAttribute("user"); 
	if(request.getSession().getAttribute("s")==null){
		s=0;
	}else {
		s=(double)request.getSession().getAttribute("s");
	}
	if(request.getSession().getAttribute("cartlistsize")==null){
		cartlistsize=0;
	}else{
		cartlistsize=(int)request.getSession().getAttribute("cartlistsize");
	}
	Ware ware = (Ware)request.getAttribute("ware");
	String inf = (String)request.getAttribute("inf");
	String info = (String)request.getAttribute("info");
	List<Ware> warelist=(List<Ware>)request.getAttribute("warelist");
	int val=(int)request.getSession().getAttribute("val");
	List<Classs>classsList=(List<Classs>)request.getAttribute("classsList");
%>
<div id="wrap">

       <div class="header">
       		<div class="logo"><a href="index.jsp"><img src="images/logo.gif" alt="" title="" border="0" /></a></div>            
        <div id="menu">
            <ul>                                                                       
            <li ><a href="index.jsp">首页</a></li>
            
            <li class="selected"><a href="${pageContext.request.contextPath }/AllSelect">花卉</a></li>
            <li><a href="${pageContext.request.contextPath }/AllBigServlet">特价礼品</a></li>
             <li>
				<%
				if(user!=null){ %>
				<a href="JumpIntoQuaryinfo">${name}</a>
				<a href="${pageContext.request.contextPath}/Logout" class="b">登出</a>
				<%} else{%>
				<a href="JumpIntoLoginServlet">登录</a>
				<a href="JumpIntoRegist" >注册</a>&nbsp;
				<%} %>
				
			</li> 
			<li><a href="JumpIntoOrder">个人订单</a></li>
            <li><SCRIPT>  
   var d = new Date();   
  document.write(d.getFullYear() + "年" +(d.getMonth() + 1) + "月" + d.getDate() + "日");   
  document.write(' 星期'+'日一二三四五六'.charAt(new Date().getDay()));   
 </SCRIPT></li>
            </ul>
        </div>     
            
            
       </div> 
       
       
       <div class="center_content">
       	<div class="left_content">
       		<link rel="stylesheet" href="css/lightbox.css" type="text/css" media="screen" />
        		<script src="js/prototype.js" type="text/javascript"></script>
				<script src="js/scriptaculous.js?load=effects" type="text/javascript"></script>
				<script src="js/lightbox.js" type="text/javascript"></script>
			    <script type="text/javascript" src="js/java.js"></script>
            <div class="crumb_nav">
            <a href="index.jsp">首页</a> &gt;&gt; <a href="${pageContext.request.contextPath }/AllSelect">产品列表</a> &gt;&gt; <%=ware.getName() %>
            </div>
            <div class="title"><span class="title_icon">
            
            </span><%=ware.getName() %></div>
        
        	<div class="feat_prod_box_details">
            
            	<div class="prod_img">
            	<a >
            	<img src="${pageContext.request.contextPath }<%="/photo/"+ware.getPicture1() %>" alt="" title="" />
            	</a>
                <br /><br />
                <a href="${pageContext.request.contextPath }<%="/photo/"+ware.getPicture1() %>" rel="lightbox">
                <img src="images/zoom.gif" alt="" title="" border="0" /></a>
                </div>
                
                <div class="prod_det_box">
                	<div class="box_top"></div>
                    <div class="box_center">
                    <div class="prod_title">简介</div>
                    <p class="details"><%=info%>  </p>
                    <div class="price"><strong>价格:</strong> <span class="red">￥ 
						<%=ware.getPrice() %></span></div>
					<div class="price"><strong>销量:</strong> <span class="black">
						<%=val %></span></div>
                    <div class="price"><strong>数量:</strong> 
                    <span><%= ware.getNum() %></span>
 
          
          
          
          
          
          
            <form action="${pageContext.request.contextPath}/AddWareIntoCartServlet" method="post">
            	<input type="text" name="wareid" style="display:none" value="<%= ware.getId() %>">
            	<input type="text" name="userid" style="display:none" value="<%= user.getId()%>">
            	<br/>
            	购买数量<input type="text" value="1" name="warenum" onkeyup="(this.v=function(){this.value=this.value.replace(/[^0-9-]+/,'');}).call(this)" onblur="this.v();" />
           		<a class="more">
           		<input type="submit" value="" style="background-image:url('images/order_now.gif');width: 100px;height: 30px;" border="1px solid #3079ED"  >
           		</a>
            </form>               
            
            
            
            
               </div>
                    
                    <div class="clear"></div>
                    </div>
                    
                    <div class="box_bottom"></div>
                </div>    
            <div class="clear"></div>
            </div>	
            
              
            <div id="demo" class="demolayout">

                <ul id="demo-nav" class="demolayout">
                <li><a class="active" href="#tab1">详细介绍</a></li>
                <li><a class="" href="#tab2">相关产品</a></li>
                </ul>
    
            <div class="tabs-container">
            
                    <div style="display: block;" class="tab" id="tab1">
                                        <p class="more_details">&nbsp;&nbsp;&nbsp; 
										<%=ware.getInfo() %></p>
                    </div>	
                    
                    <div style="display: none;" class="tab" id="tab2">
                     
                     <% for(int i=0;i<6;i++){%>
       
                    <div class="new_prod_box">
                        <a href="${pageContext.request.contextPath }/WareServlet?id=<%= warelist.get(i).getId() %>"><%= warelist.get(i).getName() %></a>
                        <div class="new_prod_bg">
                        	<span class="new_icon" >
                        		<img src="images/new_icon.gif" alt="" title="" />
                       		</span>
						<a href="${pageContext.request.contextPath }/WareServlet?id=<%= warelist.get(i).getId() %>"><img src="${pageContext.request.contextPath }<%="/photo/"+warelist.get(i).getPicture1() %>" alt="" title="" class="thumb" border="0" /></a>
                        </div>           
                    </div>
                   
                  <% }%> 
                  
                    <div class="clear"></div>
                    </div>	
            
            </div>


			</div>
			<script type="text/javascript">
				var tabber1 = new Yetii({
				id: 'demo'
				});
			</script>
          
            
        <div class="clear"></div>
        </div>
		<!--end of left content-->
        
        <div class="right_content">
                
                
              <div class="cart">
                  <div class="title"><span class="title_icon"><img src="images/cart.gif" alt="" title="" /></span>我的购物车</div>
                  <div class="home_cart_content">
                  <%=cartlistsize %> 件产品 | <span class="red">总价： ￥<%=s %></span>
                  </div>
                  <a href="JumpIntoCart" class="view_cart">查看</a>
              
              </div>
                       
            	
        
        
             <div class="title"><span class="title_icon"><img src="images/bullet3.gif" alt="" title="" /></span>关于星星花店</div> 
             <div class="about">
             <p>
             <img src="images/about.gif" alt="" title="" class="right" /> 
				星星花店全称星星花卉网上商城，是中国鲜花行业品牌服务商（曾多次次获得行业殊荣：中国电子商务协会评定为“中国互联网电子商务鲜花礼品行业龙头企业”），隶属于鲜花商城有限公司，网站自创办以来一直保持高速成长，连续四年增长率均超过100%，目前规模和影响力均位居中国鲜花礼品网站前列，销量已连续3年在同类网站中处于领先地位。  
             </p>
             
             </div>
             
             
                    
                <div class="right_box">

					<div class="title">
						<span class="title_icon"><img src="images/bullet4.gif"
							alt="" title="" /></span>促销产品
					</div>
					<div class="new_prod_box">
						<a href="details.jsp">康乃馨</a>
						<div class="new_prod_bg">
							<span class="new_icon"><img src="images/promo_icon.gif"
								alt="" title="" /></span> <a href="#"><img
								src="images/thumb1.gif" alt="" title="" class="thumb" border="0" /></a>
						</div>
					</div>

					<div class="new_prod_box">
						<a href="details.jsp">水仙花</a>
						<div class="new_prod_bg">
							<span class="new_icon"><img src="images/promo_icon.gif"
								alt="" title="" /></span><a href="#"><img
								src="images/thumb2.gif" alt="" title="" class="thumb" border="0" /></a>
						</div>
					</div>


				</div>
             
             <div class="right_box">
             
             	<div class="title"><span class="title_icon"><img src="images/bullet5.gif" alt="" title="" /></span>产品分类</div> 
                
                
			<ul class="list">
                <%for(int i=0;i<classsList.size();i++){ %>
                	<li><a href="${pageContext.request.contextPath }/WareSelect?classs=<%= classsList.get(i).getClasss_name()%> "><%= classsList.get(i).getClasss_name()%></a></li>
                <%} %>                                      
                </ul>
                
 
             
             </div>         
             
        
        </div><!--end of right content-->
        
        
       
       
       <div class="clear"></div>
       </div><!--end of center content-->
       
              
       <div class="footer">
           <p>&copy;星星花店</p>
       </div>
    

</div>

</body>

</html>