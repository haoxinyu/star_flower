﻿<%@page import="entity.Ware"%>
<%@page import="service.WareService"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="service.ClasssService"%>
<%@page import="entity.Classs"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>商品管理</title>
<style type="text/css">
	#rounded-corner thead th.rounded-company
	{
	width:26px;
	background: #60c8f2 url('images/left.jpg') left top no-repeat;
	}
	#rounded-corner thead th.rounded-q4
	{
	background: #60c8f2 url('images/right.jpg') right top no-repeat;
	}
	#rounded-corner th
	{
	padding: 8px;
	font-weight: normal;
	font-size: 13px;
	color: #039;
	background: #60c8f2;
	}
	#rounded-corner td
	{
	padding: 8px;
	background: #ecf8fd;
	border-top: 1px solid #fff;
	color: #669;
	}
	#rounded-corner tbody tr:hover td
	{
	background: #d2e7f0;
	}
	h2{
	font-size:30px;
	color:#256c89;
	font-weight:300px;
	padding: 8px;
	margin:0px;
	clear:both;
	}
	h1{
	font-size:36px;
	color:#256c89;
	font-weight:300px;
	padding: 8px;
	margin:0px;
	clear:both;
	}
	h3{
	font-size:16px;
	color:#256c89;
	font-weight:300px;
	padding: 8px;
	margin:0px;
	clear:both;
	}
	a{
	font-size:16px;
	color:#256c89;
	font-weight:300px;
	padding: 0px;
	margin:0px;
	clear:both;
	}
    body{
     background-image:  url("images/bg.jpg");
    }
     #particles-js {
        position: absolute;
        top: 0;
        width: 100%;
    }
</style>
</head>
<body>
	<%
		WareService ws=new WareService();
		ClasssService cs=new ClasssService();
		ArrayList<Classs> cc=(ArrayList<Classs>)cs.select();
	%>
	<h1 align="center">商品管理</h1>
	<br/><br/>
	<table align="center">
	<%
	int k,p=0;
	if(cc.size()%2==0){
		k=cc.size()/2;
	}
	else{
		k=cc.size()/2+1;
	}
	for(int j=0;j<k;j++){ 
	
	%>
		<tr style="width:1200px; height: 300px;top: 0px;">
		<%for(int x=0;x<2;x++){ 
			if(p<cc.size()){
				ArrayList<Ware> name=(ArrayList<Ware>)ws.selectobject(cc.get(p).getClasss_name());
			%>
			<td style="width: 600px; vertical-align:top; text-align: center;">
				<h2><%=cc.get(p).getClasss_name() %></h2>
				<table align="center" border="0" id="rounded-corner">
					<tr>
						<th class="rounded-company">商品名</th>
						<th class="rounded">是否促销</th>
						<th class="rounded">剩余数量</th>
						<th class="rounded">价格</th>
						<th class="rounded-q4">操作</th>
					</tr>
					<%for(int i=0;i<name.size();i++){ %>
						<tr>
							<td><%=name.get(i).getName() %></td>
							<td><%=name.get(i).getClassb() %></td>
							<td><%=name.get(i).getNum() %></td>
							<td><%=name.get(i).getPrice() %></td>
							<td><a href="${pageContext.request.contextPath}/Afindware?id=<%=name.get(i).getId() %>">修改商品</a>
							    &nbsp;&nbsp;&nbsp;&nbsp;
							    <a href="${pageContext.request.contextPath}/Adeleteware?id=<%=name.get(i).getId() %>" >
							    <img src="images/trash.png" alt="" title="" border="0">
							    </a></td>
						</tr>
					<%} %>
				</table>
			</td>
			<%}
			p++;
		} %>
		</tr>
	<%} %>

	</table>
	<a href="JumpIntoAnewware">新增商品</a>&nbsp;&nbsp;<a href="JumpIntoAmain">返回主页</a>
<!-- particles.js container -->
<div id="particles-js" style="z-index: -2"></div>
<!-- scripts -->
<script src="js/particles.min.js"></script>
<script src="js/app.js"></script>
<!-- stats.js -->
</body>
</html>
