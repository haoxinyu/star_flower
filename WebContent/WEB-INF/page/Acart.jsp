﻿<%@page import="service.UserService"%>
<%@page import="service.CartService"%>
<%@page import="java.util.*"%>
<%@page import="entity.*"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>订单管理</title>
<style type="text/css">
	#rounded-corner thead th.rounded-company
	{
	width:26px;
	background: #60c8f2 url('images/left.jpg') left top no-repeat;
	}
	#rounded-corner thead th.rounded-q4
	{
	background: #60c8f2 url('images/right.jpg') right top no-repeat;
	}
	#rounded-corner th
	{
	padding: 8px;
	font-weight: normal;
	font-size: 13px;
	color: #039;
	background: #60c8f2;
	}
	#rounded-corner td
	{
	padding: 8px;
	background: #ecf8fd;
	border-top: 1px solid #fff;
	color: #669;
	}
	#rounded-corner tbody tr:hover td
	{
	background: #d2e7f0;
	}
	h2{
	font-size:30px;
	color:#256c89;
	font-weight:300px;
	padding: 8px;
	margin:0px;
	clear:both;
	}
	h3{
	font-size:16px;
	color:#256c89;
	font-weight:300px;
	padding: 8px;
	margin:0px;
	clear:both;
	}
		body{
     background-image:  url("images/bg.jpg");
    }
     #particles-js {
        position: absolute;
        top: 0;
        width: 100%;
    }
</style>
</head>
<body>
	<h2 align="center">订单管理</h2>
	<br/><br/>
	<% 	List<Integer> user=new ArrayList<Integer>();
		CartService cs=new CartService();
		user=cs.selectuser();
	%>
	<%for(int i=0;i<user.size();i++){ %>
			<%  UserService us=new UserService();
				User uu=us.selectone(user.get(i));
				List<Cart> cartlist=new ArrayList<Cart>();
				cartlist=cs.finddingdan(user.get(i));
				 %>
			<table border="0" align="center" style="width: 800px;"id="rounded-corner">
				<caption>id=<%=i %>:用户信息</caption>
				<tr>
					<th >id</th>
					<th>用户名</th>
					<th>电话</th>
					<th width="300px">收货地址</th>
					<th style="width: 300; text-align: center;">操作</th>
				</tr>
				<tr>
					<td><%=user.get(i) %></td>
					<td><%=uu.getUsername() %></td>
					<td><%=uu.getPhone() %></td>
					<td><%=uu.getAddress() %></td>
					<td>
						<form action="${pageContext.request.contextPath}/Achangeaddress" method="post">
							<input type="text" name="address"/>
							<input type="text" name="id" value="<%=user.get(i)%>" style="display: none"/>
							<input type="submit" value="修改收货地址"/>
						</form>
					</td>
				</tr>
			</table>
			
			<table align="center" style="width: 800px;"id="rounded-corner">
				<caption>id=<%=i %>:用户订单</caption>
				<tr>
					<th>id</th>
					<th>商品名</th>
					<th>商品数量</th>
					<th>商品单价</th>
					<th style="width: 300; text-align: center;">商品流水</th>
				</tr>
				<%for(int j=0;j<cartlist.size();j++){ 
					if(cartlist.get(j).getType()==1){
				%>
					<tr>
						<td><%=cartlist.get(j).getWareid() %></td>
						<td><%=cartlist.get(j).getWarename()%></td>
						<td><%=cartlist.get(j).getWarenum() %></td>
						<td><%=cartlist.get(j).getWareprice()%></td>
						<td><%=cartlist.get(j).getSerialnum() %></td>
					</tr>
				<%} 
				} %>
				<tr>
					<td colspan="5">
						<form action="${pageContext.request.contextPath}/Afahuo" method="post">
							<input type="text" name="id" value="<%=user.get(i)%>" style="display: none"/>
							<input type="submit" value="发货" style="width: 100%;"/>
						</form>
				
					</td>
				</tr>
			</table>
		<br/>
	<%} %>
<a href="JumpIntoAmain"><h3>返回主页</h3></a>
	<!-- particles.js container -->
<div id="particles-js" style="z-index: -2"></div>
<!-- scripts -->
<script src="js/particles.min.js"></script>
<script src="js/app.js"></script>
<!-- stats.js -->
</body>
</html>