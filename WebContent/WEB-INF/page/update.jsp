<%@page import="entity.User"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
		#address select{width: 80px;height: 30px;border: 1px solid #c0c0c0;border-radius: 5px;outline: none;}
		#address select option{width: 80px;max-width: 100px;text-overflow: ellipsis;overflow: hidden;white-space: nowrap;}
	</style>
<style type="text/css">
#form1 {
	color: #F00;
	text-align:right;
	font-size: 20px;
}

</style>
<link rel="stylesheet" type="text/css" href="css/calendar.css">
<link rel="stylesheet" type="text/css" href="css/cs-select.css" />
<link rel="stylesheet" type="text/css" href="css/style2.css" />
<link rel="stylesheet" type="text/css" href="css/style3.css" />
<link rel="stylesheet" type="text/css" href="css/cs-skin-rotate.css" /> 
<script type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/strength.js"></script>
<script src="js/classie.js"></script>
<script src="js/selectFx.js"></script>

<script type="text/javascript">
$(document).ready(function($) {//密码输入
	$('#myPassword').strength({
		strengthClass: 'strength',
		strengthMeterClass: 'strength_meter',
		strengthButtonClass: 'button_strength',
		strengthButtonText: '显示密码',
		strengthButtonTextToggle: '隐藏密码'
	});
	(function() {
		[].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {	
			new SelectFx(el);
		} );
	})();
});
</script>
<style type="text/css">
	    #particles-js {
        position: absolute;
        top: 0;
        width: 100%;
    }
</style>
</head>

<body>
	<%User user=(User)request.getSession().getAttribute("user"); 
		/* int kk=request.getSession().getAttribute("kk"); */
	%>
	<form  method="post" action="${pageContext.request.contextPath }/UpdateServlet" id="myform" >

	<table style="border-collapse:separate;   border-spacing:20px;">
   
   		<caption style="text-align:center; font-size: 30px; border-bottom:1px solid lightgray;">信息修改</caption>

    	<tr>
       		<td width="100px">
            	用户名：
            </td>
        	<td width="200px">
                <input type="text" name="username" value="<%=user.getUsername() %>" id="textfield" style="color: #454545;"  class="reg_user"/>
            </td>
            <td width="50px">
           		 &nbsp; &nbsp; &nbsp;
 				<span id="form1" class="user_hint">*</span>
            </td>
        </tr>
        <!-- <tr>
      
        	<td>
            	密码：
            </td>
        	<td>
                <input type="password" name="password" id="myPassword" style="color: #454545;" class="reg_password"/>
            </td>
              <td width="50px">
           		 &nbsp; &nbsp; &nbsp;
 				<span id="form1" class="password_hint">*</span>
            </td>
        </tr> -->
        <tr>
        	<td>
            	年龄：
            </td>
        	<td>
    			<input type="text" name="age" value="<%=user.getAge() %>"  id="textfield3" style="color: #454545;" class="reg_age"/>
            </td>
            <td width="50px">
           		 &nbsp; &nbsp; &nbsp;
 				<span id="form1" class="age_hint">*</span>
            </td>
        </tr>
         <tr>
        	<td>
            	性别：
            </td>
        	<td >
        	 	<select name="sex1" id="select" class="cs-select cs-skin-rotate">
        	 	  <option value="Agender" >Agender-无性别</option>
        	 	  <option value="Androgyne">Androgyne-两性人</option>
        	 	  <option value="Questioning">Questioning-存疑</option>
        	 	  <option value="Pangender">Pangender-泛性别</option>
        	 	  <option value="Cis">Cis-顺性</option>
        	 	  <option value="Trans">Trans-跨性</option>
        	 	  <option value="Transsexual">Transsexual-变性</option>
                </select>

            </td>
          
        </tr>

        <tr>
        <td></td>
        	  <td>
        	  <div style="z-index:1;position: absolute;"><!-- 用iframe 解决此bug -->       
            	<select name="sex2" id="select2" class="cs-select cs-skin-rotate">
        	 	  <option value="_ma3n">男</option>
        	 	  <option value="_woman">女</option>
        	 	  <option value="_nosure">不确定</option>
                </select>
              </div>
            </td>
        </tr>
         <tr>
        	<td>
        	<span> </span>
            </td>
        	<td>
        	<span style="display: none;">sss</span>
            </td>
        </tr>
         <tr>
        	<td>
            	收货地址：
            </td>
        		 <td id="address" style="margin: 50px auto auto 50px;">
					<select class="select" id="province" name="province" style="width: 33%;float: left;">
						<option value="">选择省份</option>
					</select>
					<select name="city" id="city" style="width: 33%;float: left;">
						<option value="">选择城市</option>
					</select>
					<select name="town" id="town" style="width: 33%;float: left;">
						<option value="">选择区域</option>
					</select>
				 </td> 
				 <tr>
				 <td>
           		 </td> 
				<td>
    				<input type="text" name="address1" id="textfield3" style="color: #454545;"/>
           		 </td> 
           		   <td width="50px">
           			 &nbsp; &nbsp; &nbsp;
 					<span id="form1">*</span>
           		 </td>
          	  </tr>
         </tr>
         <tr>
        	<td>
            	联系方式：
            </td>
        	<td>
    			<input type="text" name="phone" value="<%=user.getPhone() %>" id="textfield3" style="color: #454545;" class="reg_mobile"/>
            </td>
              <td width="50px">
           		 &nbsp; &nbsp; &nbsp;
 				<span id="form1" class="mobile_hint">*</span>
            </td>
        </tr>
         <tr>
        	<td>
            	生日：
            </td>
        	<td>
    			<input type="text" id="birthday" value="<%=user.getInfo_birthday() %>" name="info_birthday" style="color: #454545;" class="reg_date"/>
            </td>
              <td width="50px">
           		 &nbsp; &nbsp; &nbsp;
 				<span id="form1" class="date_hint">*</span>
            </td>
        </tr>
                 <tr>
        	<td>
            	电子邮件：
            </td>
        	<td>
    			<input type="text" id="txtEmail" value="<%=user.getInfo_mail() %>" name="info_mail" style="color: #454545;" class="reg_email"/>
            </td>
              <td width="50px">
           		 &nbsp; &nbsp; &nbsp;
 				<span id="form1" class="email_hint">*</span>
            </td>
        </tr>
                 <tr>
        	<td>
            	身份证号：
            </td>
        	<td>
    			<input type="text" id="birthday" value="<%=user.getInfo_idnum() %>" name="info_idnum" style="color: #454545;" class="reg_idnum"/>
            </td>
              <td width="50px">
           		 &nbsp; &nbsp; &nbsp;
 				<span id="form1" class="idnum_hint">*</span>
            </td>
        </tr>
        <tr>
        	<td></td>
        	<td></td>
        	<td></td>
        </tr>
    </table>
    <input type="submit" value="修改" id="submit" class="red_button" style="background-image: url('images/login.gif'); width: 100px; height: 30px;  border: 1px solid #3079ED; opacity: 0.8; margin-right: 180px;"/>
    &nbsp;&nbsp; &nbsp;&nbsp;
    <input type="reset" value="重置" style="background-image: url('images/login.gif'); width: 100px; height: 30px;  border: 1px solid #3079ED; opacity: 0.8;"/>
    <script src="http://www.jq22.com/jquery/jquery-1.10.2.js"></script>
	<script src="js/update.js"></script>
	
	<script type="text/javascript" src="js/addres.js"></script>
	<script type="text/javascript">
		$(function(){
			$("#address").selectAddress()
			$("#town").focusout(function(){
				var province = $("#province option:selected").html()
				var city = $("#city option:selected").html()
				var town = $("#town option:selected").html()
				if(province!= '选择省份' && city!="选择城市" && town!='选择区域'){
					console.log('省份/直辖市：'+province+'\n城市:'+city+'\n区/县：'+town)
				}	
			})
		})
	</script>
</form>

<!-- particles.js container -->
<div id="particles-js" style="z-index: -2"></div>
<!-- scripts -->
<script src="js/particles.min.js"></script>
<script src="js/app.js"></script>
<!-- stats.js -->

</body>
</html>