// user
var user_Boolean = false;
var emaile_Boolean = false;
var idnum_Boolean = false;
var date_Boolean = false;
$('.reg_user').blur(function(){
  if ((/^[a-z0-9_-]{6,10}$/).test($(".reg_user").val())){
    $('.user_hint').html("✔").css("color","green");
    user_Boolean = true;
  }else {
    $('.user_hint').html("×").css("color","red");
    user_Boolean = false;
  }
});
// Email
$('.reg_email').blur(function(){
  if ((/^[a-z\d]+(\.[a-z\d]+)*@([\da-z](-[\da-z])?)+(\.{1,2}[a-z]+)+$/).test($(".reg_email").val())){
    $('.email_hint').html("✔").css("color","green");
    emaile_Boolean = true;
  }else {
    $('.email_hint').html("×").css("color","red");
    emaile_Boolean = false;
  }
});


//date
$('.reg_date').blur(function(){
  if ((/^[0-9]{4}-[0-1]?[0-9]{1}-[0-3]?[0-9]{1}$/).test($(".reg_date").val())){
    $('.date_hint').html("✔").css("color","green");
    date_Boolean = true;
  }else {
    $('.date_hint').html("×").css("color","red");
    date_Boolean = false;
  }
});

// 身份证
$('.reg_idnum').blur(function(){
  if ((/(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/).test($(".reg_idnum").val())){
    $('.idnum_hint').html("✔").css("color","green");
    idnum_Boolean = true;
  }else {
    $('.idnum_hint').html("×").css("color","red");
    idnum_Boolean = false;
  }
});


// click
$('.red_button').click(function(){
  if(date_Boolean && user_Boolean && emaile_Boolean && mobile_Boolean == true){
    alert("修改成功");
    return true;
  }else {
    alert("请正确输入后登陆");
    return false;
  }
});
