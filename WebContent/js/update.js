// user
var user_Boolean = false;
var varconfirm_Boolean = false;
var emaile_Boolean = false;
var Mobile_Boolean = false;
var idnum_Boolean =false;
var date_Boolean=false;
$('.reg_user').blur(function(){
  if ((/^[a-z0-9_-]{6,10}$/).test($(".reg_user").val())){
    $('.user_hint').html("✔").css({"color":"green","font-size":"20px"});
    user_Boolean = true;
  }else {
    $('.user_hint').html("×").css({"color":"red","font-size":"20px"});
    user_Boolean = false;
  }
});
// password
$('.reg_password').blur(function(){
  if ((/^[a-z0-9_-]{6,16}$/).test($(".reg_password").val())){
    $('.password_hint').html("✔").css({"color":"green","font-size":"20px"});
    password_Boolean = true;
  }else {
    $('.password_hint').html("×").css({"color":"red","font-size":"20px"});
    password_Boolean = false;
  }
});


// password_confirm
$('.reg_age').blur(function(){
  if ((/^(?:[1-9][0-9]?|1[01][0-9]|120)$/).test($(".reg_age").val())){
    $('.age_hint').html("✔").css({"color":"green","font-size":"20px"});
    varconfirm_Boolean = true;
  }else {
    $('.age_hint').html("×").css({"color":"red","font-size":"20px"});
    varconfirm_Boolean = false;
  }
});


// Email
$('.reg_email').blur(function(){
  if ((/^[a-z\d]+(\.[a-z\d]+)*@([\da-z](-[\da-z])?)+(\.{1,2}[a-z]+)+$/).test($(".reg_email").val())){
    $('.email_hint').html("✔").css({"color":"green","font-size":"20px"});
    emaile_Boolean = true;
  }else {
    $('.email_hint').html("×").css({"color":"red","font-size":"20px"});
    emaile_Boolean = false;
  }
});


// Mobile
$('.reg_mobile').blur(function(){
  if ((/^1[34578]\d{9}$/).test($(".reg_mobile").val())){
    $('.mobile_hint').html("✔").css({"color":"green","font-size":"20px"});
    Mobile_Boolean = true;
  }else {
    $('.mobile_hint').html("×").css({"color":"red","font-size":"20px"});
    Mobile_Boolean = false;
  }
});

//date
$('.reg_date').blur(function(){
  if ((/^[0-9]{4}-[0-1]?[0-9]{1}-[0-3]?[0-9]{1}$/).test($(".reg_date").val())){
    $('.date_hint').html("✔").css({"color":"green","font-size":"20px"});
    date_Boolean = true;
  }else {
    $('.date_hint').html("×").css({"color":"red","font-size":"20px"});
    date_Boolean = false;
  }
});

// 身份证
$('.reg_idnum').blur(function(){
  if ((/(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/).test($(".reg_idnum").val())){
    $('.idnum_hint').html("✔").css({"color":"green","font-size":"20px"});
    idnum_Boolean = true;
  }else {
    $('.idnum_hint').html("×").css({"color":"red","font-size":"20px"});
    idnum_Boolean = false;
  }
});

// click
$('.red_button').click(function(){
  if(user_Boolean && varconfirm_Boolean && emaile_Boolean && Mobile_Boolean & idnum_Boolean && date_Boolean == true){
    alert("修改成功");
    return true;
  }else {
    alert("请正确输入后登陆");
    return false;
  }
});
