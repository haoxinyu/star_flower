package entity;

import java.util.Date;
/**
 * 用户表
 * @author asus   pc
 *
 */
public class User {
	private int id;//用户id（主键）
	private String username;//用户名
	private String password;//密码
	private int age;//年龄
	private String sex;//性别
	private String address;//收货地址
	private String phone;//联系方式
	private int power;//用户权限（0： 管理员    1：普通用户）
	private Date info_birthday;//找回密码所需的信息1(生日)
	private String info_mail;//找回密码所需2（电子邮箱）
	private String info_idnum;//找回密码所需3（身份证号）
	
	public User() {
	}
	public User(int id, String username, String password, int age, String sex, String address, String phone, int power,
			Date info_birthday, String info_mail, String info_idnum) {
		this.id = id;
		this.username = username;
		this.password = password;
		this.age = age;
		this.sex = sex;
		this.address = address;
		this.phone = phone;
		this.power = power;
		this.info_birthday = info_birthday;
		this.info_mail = info_mail;
		this.info_idnum = info_idnum;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public int getPower() {
		return power;
	}
	public void setPower(int power) {
		this.power = power;
	}
	public Date getInfo_birthday() {
		return info_birthday;
	}
	public void setInfo_birthday(Date info_birthday) {
		this.info_birthday = info_birthday;
	}
	public String getInfo_mail() {
		return info_mail;
	}
	public void setInfo_mail(String info_mail) {
		this.info_mail = info_mail;
	}
	public String getInfo_idnum() {
		return info_idnum;
	}
	public void setInfo_idnum(String info_idnum) {
		this.info_idnum = info_idnum;
	}
	
	
	
}
