package entity;

import java.util.List;

/**
 * 分页pageBean
 * 
 * @author Administrator
 *
 */
public class Pager<T> {
	/**
	 * 显示页码数，默认5
	 */
	final int showPages = 5;
	/**
	 * 当前页码
	 */
	private int pageNumber;
	/**
	 * 每页显示记录数，默认为15
	 */
	private int rowsPerPage = 9;

	/**
	 * 总记录数
	 */
	private int totalRecords;
	/**
	 * 总记录数
	 */
	private int totalPages;
	/**
	 * 显示最小页码，如果是首页显示1，否则显示其他数字
	 */
	private int pageStart;
	/**
	 * 显示最大页码，可能是最后一页，也可是其他数字
	 */
	private int pageEnd;// 显示的尾页
	// pageStart = Math.max(1, pageEnd - showPages + 1);
	/**
	 * 查询结果列表
	 */
	private List<T> resultList;
	/**
	 * 分页条html文本
	 */
	private String pageLink="";

	/**
	 * 分页命令url
	 */
	private String pageUrl;
	
	public void init(){
		totalPages = totalRecords % rowsPerPage == 0 ? (totalRecords / rowsPerPage)
				: (totalRecords / rowsPerPage + 1);// 总页码数
		pageStart = Math.max(1, pageNumber - showPages / 2);// 显示的起始页码
		System.out.println("起始页码"+pageStart);
		pageEnd = Math.min(totalPages, pageStart + showPages - 1);// 显示的尾页
		System.out.println("结束页码"+pageEnd);
	}

	public Pager() {
		// TODO Auto-generated constructor stub
	}

	public List<T> getResultList() {
		return resultList;
	}

	public void setResultList(List<T> resultList) {
		this.resultList = resultList;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getRowsPerPage() {
		return rowsPerPage;
	}

	public void setRowsPerPage(int rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}

	public int getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}

	public int getTotalPages() {
		return totalRecords % rowsPerPage == 0 ? (totalRecords / rowsPerPage)
				: (totalRecords / rowsPerPage + 1);
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public int getPageStart() {
		return Math.max(1, pageNumber - showPages / 2);
	}

	public void setPageStart(int pageStart) {
		this.pageStart = pageStart;
	}

	public int getPageEnd() {
		return Math.min(totalPages, pageStart + showPages - 1);
	}

	public void setPageEnd(int pageEnd) {
		this.pageEnd = pageEnd;
	}

	public int getShowPages() {
		return showPages;
	}

	public String getPageLink() {
		String pageNum="";
		//如果已经有参数表则去掉问号，否则页码数为唯一参数
		if(this.pageUrl.contains("?")){
			pageNum="pageNum=";
		}else{
			pageNum="?pageNum=";
		}
		
		//页码大于一则需要显示前一页链接
		if (pageNumber > 1) {
			this.pageLink += "<a href=" + this.pageUrl + pageNum
					+ (pageNumber - 1) + ">&lt;上一页</a>";
		}
		
		while (pageStart <= pageEnd) {
			if (pageStart == pageNumber) {
				this.pageLink += "<b>"
					
						+ pageStart + "</b>";
			} else {
				if (pageStart % 2 != 0) {
					this.pageLink += "<a href=" + this.pageUrl + pageNum
							+ pageStart + ">"
							+ pageStart + "</a>";
				} else {
					this.pageLink += "<a href="
							+ this.pageUrl
							+ pageNum
							+ pageStart
							+ ">"
				
							+ pageStart + "</a>";
				}
			}
			pageStart++;
		}

		if (pageNumber < totalPages) {
			this.pageLink += "<a href=" + this.pageUrl + pageNum
					+ (pageNumber + 1) + ">&gt;下一页</a>";
		}

		return pageLink;
	}

	public void setPageLink(String pageLink) {
		this.pageLink = pageLink;
	}

	public String getPageUrl() {
		return pageUrl;
	}

	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}

}
