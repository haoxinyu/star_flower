package entity;
/**
 * 商品类
 * @author asus   pc
 *
 */
public class Ware {
	private int id;//商品id（主键）
	private String name;//商品名
	private String classb;//商品大分类
	private String classs;//商品小分类
	private int num;//商品存量
	private String info;//商品详细信息
	private int price;//商品价格
	private String picture1;//显示图片
	private String picture2;//显示图片
	private String picture3;//显示图片
	
	
	public Ware(int id, String name, String classb, String classs, int num, String info, int price, String picture1,
			String picture2, String picture3) {
		this.id = id;
		this.name = name;
		this.classb = classb;
		this.classs = classs;
		this.num = num;
		this.info = info;
		this.price = price;
		this.picture1 = picture1;
		this.picture2 = picture2;
		this.picture3 = picture3;
	}
	public Ware() {
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getClassb() {
		return classb;
	}
	public void setClassb(String classb) {
		this.classb = classb;
	}
	public String getClasss() {
		return classs;
	}
	public void setClasss(String classs) {
		this.classs = classs;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getPicture1() {
		return picture1;
	}
	public void setPicture1(String picture1) {
		this.picture1 = picture1;
	}
	public String getPicture2() {
		return picture2;
	}
	public void setPicture2(String picture2) {
		this.picture2 = picture2;
	}
	public String getPicture3() {
		return picture3;
	}
	public void setPicture3(String picture3) {
		this.picture3 = picture3;
	}
	
	
}
