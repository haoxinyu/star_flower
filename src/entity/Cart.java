package entity;
/**
 * 购物车表
 * @author asus   pc
 *
 */
public class Cart {
	private int userid;//用户id
	private int wareid;//商品id
	private int warenum;//商品购买数量
	private int type;//商品状态（0 ： 购物车内     1：已付款      2：已发货）
	private double wareprice;//商品价格
	private String warename;//商品名称
	private String serialnum;//流水号
	
	public Cart() {
	}
	
	public Cart(int userid, int wareid, int warenum, int type, double wareprice, String warename, String serialnum) {
		this.userid = userid;
		this.wareid = wareid;
		this.warenum = warenum;
		this.type = type;
		this.wareprice = wareprice;
		this.warename = warename;
		this.serialnum = serialnum;
	}


	public double getWareprice() {
		return wareprice;
	}





	public void setWareprice(double wareprice) {
		this.wareprice = wareprice;
	}





	public String getWarename() {
		return warename;
	}





	public void setWarename(String warename) {
		this.warename = warename;
	}





	public String getSerialnum() {
		return serialnum;
	}





	public void setSerialnum(String serialnum) {
		this.serialnum = serialnum;
	}





	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public int getWareid() {
		return wareid;
	}
	public void setWareid(int wareid) {
		this.wareid = wareid;
	}
	public int getWarenum() {
		return warenum;
	}
	public void setWarenum(int warenum) {
		this.warenum = warenum;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	
}
