package service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import dao.UserDao;
import entity.User;

public class UserService {
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public List<User> selectAll(){
		UserDao userdao=new UserDao();
		List<User> all=new ArrayList<User>();
		all=userdao.selectAll();
		return all;
	}
	public int powerup(int id){
		UserDao userdao=new UserDao();
		int pa=userdao.powerup(id);
		return  pa;
	}
	public int delete(int id){
		UserDao userdao=new UserDao();
		int pa=userdao.delete(id);
		return  pa;
	}
///////////////////////////////////////////////////////////////////////////////
	
//----------------------------------------------------------------
	
	public User login(String username,String password){
		UserDao userdao=new UserDao();
		return userdao.login(username, password);
		
	}
	public User quaryInfo(int id){
		UserDao userdao=new UserDao();
		User user=null;
		user=userdao.quaryInfo(id);
		return user;
	}
	public void update(User user){
		UserDao userdao=new UserDao();
		userdao.update(user);
	}
	public int selectMaxId(){
		UserDao userdao=new UserDao();
		int maxid=0;
		maxid=userdao.selectMaxId();
		return  maxid;
	}
	public void registor(User user){
		UserDao userdao=new UserDao();
		userdao.registor(user);
	}
	public User updatePassword(Date info_birthday,String info_mail,String info_idnum){
		UserDao userdao=new UserDao();
		User user=userdao.updatePassword(info_birthday,info_mail,info_idnum);
		return user;
	}
	public void password(String password,int id){
		UserDao userdao=new UserDao();
		userdao.password(password, id);
	}
//-----------------------------------------------------------------------------------------
	public User selectone(int id){
		UserDao userdao=new UserDao();
		return userdao.selectone(id);
	}
	//////////!!!!!!!//////
	public int changeaddress(int id,String address){
		UserDao userdao=new UserDao();
		return userdao.changeaddress(id, address);
	}
}
