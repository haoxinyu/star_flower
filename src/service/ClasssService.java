package service;

import java.util.List;

import dao.ClasssDao;
import entity.Classs;

public class ClasssService {
	public List<Classs> findAll(){
		ClasssDao classsDao = new ClasssDao();
		return classsDao.findAllClasss();
	}
	ClasssDao cd=new ClasssDao();
	public List<Classs> select(){
		return cd.selectClasss();
	}
	
	public void add(Classs cc) {
		cd.addClasss(cc);
	}
	
	public void delete(Classs cc) {
		cd.deleteClasss(cc);
	}
	public Classs find(int cc) {
		return cd.findClasss(cc);
	}
}
