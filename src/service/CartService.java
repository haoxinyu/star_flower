package service;

import java.sql.SQLException;
import java.util.List;

import dao.CartDao;
import dao.WareDao;
import entity.Cart;
import entity.Ware;

public class CartService {
	public void add(Cart cart){
		WareDao waredao=new WareDao();
		Ware ware=waredao.findPriceAndNameAndNum(cart.getWareid());
		cart.setWareprice(ware.getPrice());
		cart.setWarename(ware.getName());
		int warenum=ware.getNum()-cart.getWarenum();
		if(warenum>=0){
			waredao.updatewarenum(cart.getWareid(), warenum);
			CartDao cartdao=new CartDao();
			cartdao.add(cart);
		}
		
	}
	public List<Cart> findallcarts(Cart cart){
		CartDao cartdao=new CartDao();
		return cartdao.findAllCarts(cart);
	}
	
	public void deletecart(String serialnum){
		CartDao cartdao=new CartDao();
		cartdao.deleteCart(serialnum);
	}
	
	public void changecarttype(String serialnum){
		CartDao cartdao=new CartDao();
		cartdao.changeCartType(serialnum);
	}
	public List<Integer> selectuser(){
		CartDao cartdao=new CartDao();
		return cartdao.selectuser();
	}
	public void fahuo(List<Cart> cartlist){
		CartDao cartdao=new CartDao();
		for(int i=0;i<cartlist.size();i++){
			cartdao.fahuo(cartlist.get(i).getSerialnum());
		}
	}
	public List<Cart> finddingdan(int userid){
		CartDao cartdao=new CartDao();
		return cartdao.finddingdan(userid);
	}
	public Cart CSelectOrder(String serialnum){
		CartDao cartdao=new CartDao();
		Cart cart=null;
		try {
			cart = cartdao.CSelectOrder(serialnum);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cart;
		}
	
	public int SelectPCount(int wareid){
		System.out.println("我开始找销量了");
		CartDao cartdao=new CartDao();
		List<Integer> c=null;
		try {
			c = cartdao.SelectPCount(wareid);
			System.out.println("销量数组大小："+c.size());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*System.out.println("我进srver层了");
		System.out.println(c.size());*/
		int val=0;
		for(int i=0;i<c.size();i++){
			val=val+c.get(i);
			System.out.println(" ()"+val);
		}
		return val;
	}
	public List<Cart> finddingdan1(int userid){
		CartDao cartdao=new CartDao();
		return cartdao.finddingdan1(userid);
	}
}
