package service;

import java.util.List;

import dao.WareDao;
import entity.Cart;
import entity.Ware;

public class WareService {
	public  List<Ware> selectobject(String name){
		WareDao wd=new WareDao();
		return wd.selectObject(name);
	}
	public  Ware findone(int id){
		WareDao wd=new WareDao();
		//Ware ww=new Ware();
		return wd.findone(id);
	}
	public  int delete(int id){
		WareDao wd=new WareDao();
		return wd.delete(id);
	}
	public int update(Ware ww){
		WareDao wd=new WareDao();
		return wd.update(ww);
	}
	public int add(Ware ww){
		WareDao wd=new WareDao();
		return wd.add(ww);
	}
	public List find(String cla,int f){
		WareDao waredao = new WareDao();
		return waredao.find(cla,f);
	}
	public Ware query(int id){
		WareDao waredao = new WareDao();
		return waredao.query(id);
	}
	public List all() {
		WareDao waredao = new WareDao();
		return waredao.all();
	}
	public List<Ware> queryAllWareByPage(int pageNumber, int rowsPerPage){
		WareDao waredao = new WareDao();
		return waredao.queryAllWareByPage(pageNumber, rowsPerPage);
	}

}
