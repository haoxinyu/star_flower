package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import entity.Ware;


public class WareDao {
	/**
	 * 
	 * @param id
	 * @return
	 */
	public Ware findPriceAndNameAndNum(int id){
		String sql="select * from dr_ware where id=? ";
		Connection conn=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		conn=DbUtil.getDbutil().getConnection();
		int price = 0;
		String name = "";
		String zhekou;
		int num=0;
		Ware ware=null;
		try {
			pstmt=conn.prepareStatement(sql);
			pstmt.setInt(1,id);
			rs=pstmt.executeQuery();
			if(rs.next()){
				price=rs.getInt("price");
				name=rs.getString("name");
				num=rs.getInt("num");
				zhekou=rs.getString("classb");
				if(zhekou.equals("无折扣")){
					
				}
				else{
				double p=(Double.parseDouble(zhekou.charAt(0)+"")*10+Double.parseDouble(zhekou.charAt(1)+""))/100;
				System.out.println(p);
				price=(int) (price*p);
				}
			}
			ware=new Ware(0, name, "", "", num, "", price, "", "", "");
		} catch (SQLException  e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		finally{
			if(rs!=null){
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO 自动生成的 catch 块
					e.printStackTrace();
				}
			}
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return ware;
	}
	
	public void updatewarenum(int id,int num){
		String sql="update dr_ware set num=? where id=?";
		Connection conn=DbUtil.getDbutil().getConnection();
		PreparedStatement pstmt=null;
		try {
			pstmt=conn.prepareStatement(sql);
			pstmt.setInt(1, num);
			pstmt.setInt(2, id);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	public int findnum(int id){
		String sql="select num from dr_ware where id=? ";
		Connection conn=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		conn=DbUtil.getDbutil().getConnection();
		int num=0;
		try {
			pstmt=conn.prepareStatement(sql);
			pstmt.setInt(1,id);
			rs=pstmt.executeQuery();
			if(rs.next()){
				num=rs.getInt("num");
			}
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		finally{
			if(rs!=null){
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO 自动生成的 catch 块
					e.printStackTrace();
				}
			}
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return num;
	}
	
	
	//-------------------------------------------------------------------------------------
	
	public List<Ware> selectObject(String name){
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		List<Ware> ll=new ArrayList<Ware>();
		String sql="select * from dr_ware where classs=?";
		try {
			pstmt=DbUtil.getDbutil().getConnection().prepareStatement(sql);
			pstmt.setString(1, name);
			rs=pstmt.executeQuery();
			while(rs.next()){
				Ware w=new Ware();
				w.setId(rs.getInt("id"));
				w.setName(rs.getString("name"));
				w.setClassb(rs.getString("classb"));
				w.setClasss(rs.getString("classs"));
				w.setNum(rs.getInt("num"));
				w.setInfo(rs.getString("info"));
				w.setPrice(rs.getInt("price"));
				w.setPicture1(rs.getString("picture1"));
				w.setPicture2(rs.getString("picture2"));
				w.setPicture3(rs.getString("picture3"));
				ll.add(w);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ll;	
	}
	public Ware findone(int id){
		String sql="select * from dr_ware where id=? ";
		PreparedStatement pstmt=null;
		ResultSet rs=null;

		Ware ware=new Ware();
		try {
			pstmt=DbUtil.getDbutil().getConnection().prepareStatement(sql);
			pstmt.setInt(1,id);
			rs=pstmt.executeQuery();
			if(rs.next()){
				ware.setId(rs.getInt("id"));
				ware.setName(rs.getString("name"));
				ware.setNum(rs.getInt("num"));
				ware.setInfo(rs.getString("info"));
				ware.setPrice(rs.getInt("price"));
				ware.setClassb(rs.getString("classb"));
				ware.setClasss(rs.getString("classs"));
				ware.setPicture1(rs.getString("picture1"));
				ware.setPicture2(rs.getString("picture2"));
				ware.setPicture3(rs.getString("picture3"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			if(rs!=null){
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return ware;
	}
	public int update(Ware ww){
		String sql="update dr_ware set name=?,classb=?,classs=?,num=?,info=?,price=?,picture1=?,picture2=?,picture3=? where id=? ";
	  //String sql="update dr_user set username=?,password=?,age=?,sex=?,address=?,phone=?,power=?,info_birthday=?,info_mail=?,info_idnum=? where id=?";
		PreparedStatement pstmt=null;
		int zz=-1;
		try {
			//String sql=new String(sqll.getBytes("iso8859-1"),"utf-8");
			pstmt=DbUtil.getDbutil().getConnection().prepareStatement(sql);
			pstmt.setString(1,ww.getName());/**/
			pstmt.setString(2, ww.getClassb()/**/);
			pstmt.setString(3, ww.getClasss()/**/);
			pstmt.setInt(4, ww.getNum());
			pstmt.setString(5, ww.getInfo()/**/);
			pstmt.setInt(6, ww.getPrice());
			pstmt.setString(7, ww.getPicture1());
			pstmt.setString(8, ww.getPicture2());
			pstmt.setString(9, ww.getPicture3());
			pstmt.setInt(10, ww.getId());
			System.out.println(ww.getId()+" "+ww.getName()+" "+ww.getClassb()+" "+ww.getClasss());
			System.out.println(ww.getNum()+" "+ww.getInfo()+" "+ww.getPrice()+" ");
			System.out.println(ww.getPicture1()+" "+ww.getPicture2()+" "+ww.getPicture3());
			zz=pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return zz;
	}
	public int add(Ware ww){
		String sql="insert into dr_ware(id,name,classb,classs,num,info,price,picture1,picture2,picture3) values(?,?,?,?,?,?,?,?,?,?) ";
		System.out.println("dsffds"+ww.getName());
		PreparedStatement pstmt=null;
		int zz=-1;
		try {
			//String sql=new String(sqll.getBytes("iso8859-1"),"utf-8");
			pstmt=DbUtil.getDbutil().getConnection().prepareStatement(sql);
			
			pstmt.setInt(1,this.max()+1);
			pstmt.setString(2,ww.getName());/**/
			pstmt.setString(3, ww.getClassb()/**/);
			pstmt.setString(4, ww.getClasss()/**/);
			pstmt.setInt(5, ww.getNum());
			pstmt.setString(6, ww.getInfo()/**/);
			pstmt.setInt(7, ww.getPrice());
			pstmt.setString(8, ww.getPicture1());
			pstmt.setString(9, ww.getPicture2());
			pstmt.setString(10, ww.getPicture3());	
			
			/*System.out.println(ww.getId()+" "+ww.getName()+" "+ww.getClassb()+" "+ww.getClasss());
			System.out.println(ww.getNum()+" "+ww.getInfo()+" "+ww.getPrice()+" ");
			System.out.println(ww.getPicture1()+" "+ww.getPicture2()+" "+ww.getPicture3());*/
			zz=pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return zz;
	}
	public int max(){
		String sql="select max(id) id from dr_ware";
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		int zz=-1;
		try {
			pstmt=DbUtil.getDbutil().getConnection().prepareStatement(sql);
			rs=pstmt.executeQuery();
			while(rs.next()){
				zz=rs.getInt("id");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return zz;
	}
	public int delete(int id){
		String sql="delete from dr_ware where id=?";
		PreparedStatement pstmt=null;	
		int zz=-1;
		try {
			pstmt=DbUtil.getDbutil().getConnection().prepareStatement(sql);
			pstmt.setInt(1, id);
			zz=pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return zz;
	}
	//////////////////////////////////li////////////////////
	public List find(String cla,int f){		//分类查找

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Ware ware = null;
		String sql = null;
		if(f==1){
			sql = "select * from dr_ware where classb <> ?";
		}else {
			sql ="select * from dr_ware where classs = ?";
		}
		List<Ware> list = new ArrayList<Ware>();;
		try {
			pstmt = DbUtil.getDbutil().getConnection().prepareStatement(sql);
			pstmt.setString(1, cla);
			rs = pstmt.executeQuery();
			while(rs.next()){
				int id = rs.getInt("id");
				String username = rs.getString("name");
				int num = rs.getInt("num");
				String info = rs.getString("info");
				int price = rs.getInt("price");
				String classb = rs.getString("classb");
				String classs = rs.getString("classs");
				String pictuer1 = rs.getString("picture1");
				String pictuer2 = rs.getString("picture2");
				String pictuer3 = rs.getString("picture3");
				ware = new Ware();
				ware.setId(id);
				ware.setName(username);
				ware.setClassb(classb);
				ware.setClasss(classs);
				ware.setNum(num);
				ware.setInfo(info);
				ware.setPrice(price);
				ware.setPicture1(pictuer1);
				ware.setPicture2(pictuer2);
				ware.setPicture3(pictuer3);
				list.add(ware);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if(rs!=null){
					rs.close();
				}
				if(pstmt!=null){
					pstmt.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return list;
		
	}
	
	public Ware query(int id){		//查询商品详细信息

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Ware ware = null;
		String sql = "select * from dr_ware where id = ?";
		try {
			pstmt = DbUtil.getDbutil().getConnection().prepareStatement(sql);
			pstmt.setInt(1, id);
			rs = pstmt.executeQuery();
			while(rs.next()){
				String username = rs.getString("name");
				int num = rs.getInt("num");
				String info = rs.getString("info");
				int price = rs.getInt("price");
				String classb = rs.getString("classb");
				String classs = rs.getString("classs");
				String pictuer1 = rs.getString("picture1");
				String pictuer2 = rs.getString("picture2");
				String pictuer3 = rs.getString("picture3");
				ware = new Ware();
				ware.setId(id);
				ware.setName(username);
				ware.setClassb(classb);
				ware.setClasss(classs);
				ware.setNum(num);
				ware.setInfo(info);
				ware.setPrice(price);
				ware.setPicture1(pictuer1);
				ware.setPicture2(pictuer2);
				ware.setPicture3(pictuer3);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if(rs!=null){
					rs.close();
				}
				if(pstmt!=null){
					
					pstmt.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return ware;
	}
	/**
	 * 分页查找全部
	 * @param pageNumber
	 * @param rowsPerPage
	 * @return
	 */
	public List<Ware> queryAllWareByPage(int pageNumber, int rowsPerPage) {
		try {
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			Ware ware = null;
			pstmt = DbUtil.getDbutil().getConnection().prepareStatement("select * from dr_ware limit ?,?");
			pstmt.setInt(1, (pageNumber-1)*rowsPerPage);
			pstmt.setInt(2, rowsPerPage);
			rs = pstmt.executeQuery();
			List<Ware> list = new ArrayList<Ware>(); 
			while(rs.next()){
				int id = rs.getInt("id");
				String username = rs.getString("name");
				int num = rs.getInt("num");
				String info = rs.getString("info");
				int price = rs.getInt("price");
				String classb = rs.getString("classb");
				String classs = rs.getString("classs");
				String pictuer1 = rs.getString("picture1");
				String pictuer2 = rs.getString("picture2");
				String pictuer3 = rs.getString("picture3");
				ware = new Ware();
				ware.setId(id);
				ware.setName(username);
				ware.setClassb(classb);
				ware.setClasss(classs);
				ware.setNum(num);
				ware.setInfo(info);
				ware.setPrice(price);
				ware.setPicture1(pictuer1);
				ware.setPicture2(pictuer2);
				ware.setPicture3(pictuer3);
				list.add(ware);
			}
			
			return list;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;			
		}
	}
	public List<Ware> all() {
		List<Ware> list = new ArrayList<Ware>();

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Ware ware = null;
		String sql = "select * from dr_ware";
		try {
			pstmt = DbUtil.getDbutil().getConnection().prepareStatement(sql);
			rs = pstmt.executeQuery();
			while(rs.next()){
				int id = rs.getInt("id");
				String username = rs.getString("name");
				int num = rs.getInt("num");
				String info = rs.getString("info");
				int price = rs.getInt("price");
				String classb = rs.getString("classb");
				String classs = rs.getString("classs");
				String pictuer1 = rs.getString("picture1");
				String pictuer2 = rs.getString("picture2");
				String pictuer3 = rs.getString("picture3");
				ware = new Ware();
				ware.setId(id);
				ware.setName(username);
				ware.setClassb(classb);
				ware.setClasss(classs);
				ware.setNum(num);
				ware.setInfo(info);
				ware.setPrice(price);
				ware.setPicture1(pictuer1);
				ware.setPicture2(pictuer2);
				ware.setPicture3(pictuer3);
				list.add(ware);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if(rs!=null){
					rs.close();
				}
				if(pstmt!=null){
					pstmt.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return list;
	}
	
}
