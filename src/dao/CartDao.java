package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import entity.Cart;

public class CartDao {
	/**
	 * 向购物车中添加新的商品
	 * @param cart
	 */
	public void add(Cart cart){
		Connection conn=DbUtil.getDbutil().getConnection();
		String sql = "insert into dr_cart values(?,?,?,?,?,?,?)";
		PreparedStatement pstmt =null;
		String serialnum=UUID.randomUUID().toString();
		try {
			pstmt= conn.prepareStatement(sql);
			pstmt.setInt(1, cart.getUserid());
			pstmt.setInt(2, cart.getWareid());
			pstmt.setInt(3, cart.getWarenum());
			pstmt.setInt(4, cart.getType());
			pstmt.setDouble(5, cart.getWareprice());
			pstmt.setString(6, cart.getWarename());
			pstmt.setString(7, serialnum);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * 查找某用户收藏到购物车的全部商品
	 * @param cart
	 * @return
	 */
	public List<Cart> findAllCarts(Cart cart){
		//此处经过修改
		String sql="select * from dr_cart where userid=? and type=0";
		Connection conn=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		conn=DbUtil.getDbutil().getConnection();
		ArrayList<Cart> cartlist=new ArrayList<Cart>();
		try {
			pstmt=conn.prepareStatement(sql);
			pstmt.setInt(1,cart.getUserid());
//			pstmt.setInt(2,cart.getWareid());
			rs=pstmt.executeQuery();
			while(rs.next()){
				Cart cartt=new Cart();
				cartt.setUserid(rs.getInt("userid"));
				cartt.setWareid(rs.getInt("wareid"));
				cartt.setWarenum(rs.getInt("warenum"));
				cartt.setType(rs.getInt("type"));
				cartt.setWareprice(rs.getDouble("wareprice"));
				cartt.setWarename(rs.getString("warename"));
				cartt.setSerialnum(rs.getString("serialnum"));
				cartlist.add(cartt);
			}
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		finally{
			if(rs!=null){
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO 自动生成的 catch 块
					e.printStackTrace();
				}
			}
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return cartlist;
	}
	
	/**
	 * 删除购物车中的商品
	 * @param serialnum
	 */
	public void deleteCart(String serialnum){
			String sql="delete from dr_cart where serialnum=?";
			PreparedStatement pstmt=null;
			Connection conn=null;
			conn=DbUtil.getDbutil().getConnection();
			try {
				pstmt=conn.prepareStatement(sql);
				pstmt.setString(1, serialnum);
				pstmt.executeUpdate();
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			finally{
				if(pstmt!=null){
					try {
						pstmt.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if(conn!=null){
					try {
						conn.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
	}
	/**
	 * 支付后的状态改变
	 * @param serialnum
	 */
	public void changeCartType(String serialnum){
		String sql="update dr_cart set type=1 where serialnum=?";
		PreparedStatement pstmt=null;
		Connection conn=null;
		conn=DbUtil.getDbutil().getConnection();
		try {
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, serialnum);
			pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	public List<Integer> selectuser(){
		String sql="select distinct userid  id from dr_cart";
		Connection conn=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		List<Integer> zz=new ArrayList<Integer>();
		conn=DbUtil.getDbutil().getConnection();
		try {
			pstmt=conn.prepareStatement(sql);
			rs=pstmt.executeQuery();
			while(rs.next()){
				zz.add(Integer.parseInt(rs.getString("id")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return zz;
	}
	
	public void fahuo(String serialnum){
		String sql="update dr_cart set type=2 where serialnum=?";
		PreparedStatement pstmt=null;
		Connection conn=null;
		conn=DbUtil.getDbutil().getConnection();
		try {
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, serialnum);
			pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	public List<Cart> finddingdan(int userid){
		String sql="select * from dr_cart where userid=? and type=1";
		Connection conn=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		conn=DbUtil.getDbutil().getConnection();
		ArrayList<Cart> cartlist=new ArrayList<Cart>();
		try {
			pstmt=conn.prepareStatement(sql);
			pstmt.setInt(1,userid);
			rs=pstmt.executeQuery();
			while(rs.next()){
				Cart cartt=new Cart();
				cartt.setUserid(rs.getInt("userid"));
				cartt.setWareid(rs.getInt("wareid"));
				cartt.setWarenum(rs.getInt("warenum"));
				cartt.setType(rs.getInt("type"));
				cartt.setWareprice(rs.getDouble("wareprice"));
				cartt.setWarename(rs.getString("warename"));
				cartt.setSerialnum(rs.getString("serialnum"));
				cartlist.add(cartt);
			}
		} catch (SQLException e) {
			// TODO 锟皆讹拷锟斤拷锟缴碉拷 catch 锟斤拷
			e.printStackTrace();
		}
		finally{
			if(rs!=null){
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO 锟皆讹拷锟斤拷锟缴碉拷 catch 锟斤拷
					e.printStackTrace();
				}
			}
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return cartlist;
	}
	public Cart CSelectOrder(String serialnum) throws SQLException{//自己的
		Connection conn=DbUtil.getDbutil().getConnection();
		ResultSet rs=null;
		PreparedStatement pstmt=null;
		String sql="select * from dr_cart where serialnum=?";
		Cart cart=null;
		try {
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, serialnum);
			rs=pstmt.executeQuery();
			if(rs.next()){
				cart=new Cart();
				cart.setSerialnum(rs.getString("serialnum"));
				cart.setWarename(rs.getString("warename"));
				cart.setWarenum(rs.getInt("warenum"));
				cart.setWareprice(rs.getDouble("wareprice"));
				cart.setType(rs.getInt("type"));
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(rs!=null){
				rs.close();
			}
			if(pstmt!=null){
				pstmt.close();
			}
			if(conn!=null){
				conn.close();
			}
		}
		
		return cart;
	}
	public  List SelectPCount(int wareid) throws SQLException{//自己的
		Connection conn=DbUtil.getDbutil().getConnection();
		ResultSet rs=null;
		PreparedStatement pstmt=null;
		String sql="select warenum from dr_cart where wareid=? and type=2";
		System.out.println(wareid);
		List<Integer> list=new ArrayList<Integer>();
		
		try {
			pstmt=conn.prepareStatement(sql);
			pstmt.setInt(1, wareid);
			rs=pstmt.executeQuery();
			while(rs.next()){
				int k=rs.getInt("warenum");
				list.add(k);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(rs!=null){
				rs.close();
			}
			if(pstmt!=null){
				pstmt.close();
			}
			if(conn!=null){
				conn.close();
			}
		}
		
		return list;
	}
	
	public List<Cart> finddingdan1(int userid){
		String sql="select * from dr_cart where userid=? and type>0";
		Connection conn=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		conn=DbUtil.getDbutil().getConnection();
		ArrayList<Cart> cartlist=new ArrayList<Cart>();
		try {
			pstmt=conn.prepareStatement(sql);
			pstmt.setInt(1,userid);
			rs=pstmt.executeQuery();
			while(rs.next()){
				Cart cartt=new Cart();
				cartt.setUserid(rs.getInt("userid"));
				cartt.setWareid(rs.getInt("wareid"));
				cartt.setWarenum(rs.getInt("warenum"));
				cartt.setWareprice(rs.getDouble("wareprice"));
				cartt.setWarename(rs.getString("warename"));
				cartt.setSerialnum(rs.getString("serialnum"));
				cartlist.add(cartt);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			if(rs!=null){
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return cartlist;
	}

}
