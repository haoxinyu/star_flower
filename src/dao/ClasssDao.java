package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import entity.Classs;

public class ClasssDao {
	/**
	 * 无条件查询所有大类
	 * @param classs
	 * @return
	 */
	public List<Classs> findAllClasss(){
		String sql="select * from dr_classs";
		Connection conn=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		conn=DbUtil.getDbutil().getConnection();
		ArrayList<Classs> classslist=new ArrayList<Classs>();
		try {
			pstmt=conn.prepareStatement(sql);
			rs=pstmt.executeQuery();
			while(rs.next()){
				Classs classs=new Classs();
				classs.setClasss_num(rs.getInt("classs_num"));
				classs.setClasss_name(rs.getString("classs_name"));
				classslist.add(classs);
			}
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		finally{
			if(rs!=null){
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO 自动生成的 catch 块
					e.printStackTrace();
				}
			}
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return classslist;
	}
	public Classs findClasss(int cc){
		Classs xx=new Classs();
		String sql="select * from dr_classs where classs_num="+cc;
		Connection conn=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		conn=DbUtil.getDbutil().getConnection();
		try {
			pstmt=conn.prepareStatement(sql);
			rs=pstmt.executeQuery();
			while(rs.next()) {
				xx.setClasss_num(rs.getInt("classs_num"));
				xx.setClasss_name(rs.getString("classs_name"));
			}
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		return xx;
	}
	public List<Classs> selectClasss(){
		List<Classs> xx=new ArrayList<Classs>();
		String sql="select * from dr_classs";
		Connection conn=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		conn=DbUtil.getDbutil().getConnection();
		try {
			pstmt=conn.prepareStatement(sql);
			rs=pstmt.executeQuery();
			while(rs.next()) {
				Classs cc=new Classs();
				cc.setClasss_num(rs.getInt("classs_num"));
				cc.setClasss_name(rs.getString("classs_name"));
				xx.add(cc);
			}
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		return xx;
	}
	public void addClasss(Classs cc){
		String sql="insert into dr_classs value(?,?)";
		Connection conn=null;
		PreparedStatement pstmt=null;
		conn=DbUtil.getDbutil().getConnection();
		try {
			pstmt=conn.prepareStatement(sql);
			pstmt.setInt(1, this.max()+1);
			pstmt.setString(2, cc.getClasss_name());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}
	public void deleteClasss(Classs cc){
		String sql1="delete from dr_classs where classs_num=?";
		String sql2="delete from dr_ware where classs=?";
		Connection conn=null;
		PreparedStatement pstmt=null;
		Connection conn1=null;
		PreparedStatement pstmt1=null;
		conn=DbUtil.getDbutil().getConnection();
		conn1=DbUtil.getDbutil().getConnection();
		try {
			pstmt=conn.prepareStatement(sql1);
			pstmt.setInt(1, cc.getClasss_num());
			pstmt.executeUpdate();
			//删除对应类商品
			pstmt1=conn1.prepareStatement(sql2);
			pstmt1.setString(1, cc.getClasss_name());
			pstmt1.executeUpdate();
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}
	public int max(){
		String sql="select max(classs_num) id from dr_classs";
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		int zz=-1;
		try {
			pstmt=DbUtil.getDbutil().getConnection().prepareStatement(sql);
			rs=pstmt.executeQuery();
			while(rs.next()){
				zz=rs.getInt("id");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return zz;
	}
	
}
