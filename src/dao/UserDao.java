package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import dao.DbUtil;
import entity.User;

public class UserDao {
	/**
	 * 用户注册
	 * @param user
	 */
	public void registor(User user){
		Connection conn=null;
		conn=DbUtil.getDbutil().getConnection();
		PreparedStatement pstmt=null;
		String sql="insert into dr_user(id,username,password,sex,age,phone,address,power,info_birthday,info_mail,info_idnum) values(?,?,?,?,?,?,?,1,?,?,?)";
		try {
			pstmt=conn.prepareStatement(sql);
			pstmt.setInt(1, user.getId());
			pstmt.setString(2, user.getUsername());	
			pstmt.setString(3,user.getPassword());
			pstmt.setString(4, user.getSex());
			pstmt.setInt(5,user.getAge());
			pstmt.setString(6, user.getPhone());
			pstmt.setString(7, user.getAddress());
			java.util.Date date = user.getInfo_birthday();
			pstmt.setDate(8, new java.sql.Date(date.getTime()));
			pstmt.setString(9, user.getInfo_mail());
			pstmt.setString(10, user.getInfo_idnum());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			try {
				if(pstmt!=null)
					pstmt.close();
				if(conn!=null)
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	/**
	 * 查找数据库中最大的id
	 * @return
	 */
	public  int selectMaxId(){
		Connection conn=null;
		conn=DbUtil.getDbutil().getConnection();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		String sql="select Max(id) from dr_user";
		int maxid=0;
		try {
			pstmt=conn.prepareStatement(sql);
			rs=pstmt.executeQuery();
			if(rs.next()){
				maxid=rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			
				try {
					if(rs!=null)
						rs.close();
					if(pstmt!=null)
						pstmt.close();
					if(conn!=null)
						conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
		}
		
			return maxid;
	}
	/**
	 * 管理员查询所有用户信息
	 * @return
	 */
	public  List<User> selectAll(){
		Connection conn=DbUtil.getDbutil().getConnection();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		List<User> zz=new ArrayList<User>();
		String sql="select * from dr_user where power=1";
		try {
			pstmt=conn.prepareStatement(sql);
			rs=pstmt.executeQuery();
		
			while(rs.next()){
				User p=new User();
				p.setId(rs.getInt("id"));
				p.setUsername(rs.getString("username"));
				p.setAge(rs.getInt("age"));
				p.setSex(rs.getString("sex"));
				p.setPassword(rs.getString("password"));
				p.setInfo_birthday(rs.getDate("info_birthday"));
				p.setAddress(rs.getString("address"));
				p.setPhone(rs.getString("phone"));
				p.setInfo_mail(rs.getString("info_mail"));
				p.setInfo_idnum(rs.getString("info_idnum"));
				zz.add(p);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			
				try {
					if(rs!=null)
						rs.close();
					if(pstmt!=null)
						pstmt.close();
					if(conn!=null)
						conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
		}
			return zz;
	}
	/**
	 * 登录
	 * @param username
	 * @param password
	 * @return
	 */
	public  User login(String username,String password){
		Connection conn=null;
		conn=DbUtil.getDbutil().getConnection();
		ResultSet rs=null;
		PreparedStatement pstmt=null;
		String sql="select * from dr_user where username=? and password=?";
		User user = null;
		try {
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, username);
			pstmt.setString(2, password);
			rs=pstmt.executeQuery();
			while(rs.next()){
				user = new User();
				user.setId(rs.getInt("id"));
				user.setAge(rs.getInt("age"));
				user.setPower(rs.getInt("power"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				user.setPhone(rs.getString("phone"));
				user.setAddress(rs.getString("address"));
				user.setSex(rs.getString("sex"));
				user.setInfo_birthday(rs.getDate("info_birthday"));
				user.setInfo_mail(rs.getString("info_mail"));
				user.setInfo_idnum(rs.getString("info_idnum"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(conn!=null)
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return user;
	}
	/**
	 * 将普通用户提升至管理员
	 * @param id
	 * @return
	 */
	public int powerup(int id){
		Connection conn=DbUtil.getDbutil().getConnection();
		PreparedStatement pstmt=null;
		int zz=-1;
		String sql="update dr_user set power=0 where id=?";
		try {
			/*System.out.println(id);*/
			pstmt=conn.prepareStatement(sql);
			pstmt.setInt(1, id);
			zz=pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			try {
				if(pstmt!=null)
					pstmt.close();
				if(conn!=null)
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return zz;
	}
	/**
	 * 管理员删除用户
	 * @param id
	 * @return
	 */
	public int delete(int id){
		Connection conn=DbUtil.getDbutil().getConnection();
		PreparedStatement pstmt=null;
		int zz=-1;
		String sql="delete from dr_user where id=?";
		try {
			/*System.out.println(id);*/
			pstmt=conn.prepareStatement(sql);
			pstmt.setInt(1, id);
			zz=pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			try {
				if(pstmt!=null)
					pstmt.close();
				if(conn!=null)
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return zz;
	}
	/**
	 * 通过用户id查询它的所有信息
	 * @param id
	 * @return
	 */
	public User quaryInfo(int id){
		Connection conn=null;
		conn=DbUtil.getDbutil().getConnection();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		User user=null;
		String sql="select * from dr_user where id=?";
		try {
			pstmt=conn.prepareStatement(sql);
			pstmt.setInt(1, id);
			rs=pstmt.executeQuery();
			if(rs.next()){
				user=new User();
				user = new User();
				//user.setId(rs.getInt("id"));
				user.setAge(rs.getInt("age"));
				user.setPower(rs.getInt("power"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				user.setPhone(rs.getString("phone"));
				user.setAddress(rs.getString("address"));
				user.setSex(rs.getString("sex"));
				user.setInfo_birthday(rs.getDate("info_birthday"));
				user.setInfo_mail(rs.getString("info_mail"));
				user.setInfo_idnum(rs.getString("info_idnum"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(conn!=null)
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return user;
	}
	/**
	 * 修改用户信息
	 * @param user
	 */
	public void update(User user){
		Connection conn=null;
		conn=DbUtil.getDbutil().getConnection();
		PreparedStatement pstmt=null;
		String sql="update dr_user set username=?,age=?,sex=?,address=?,phone=?,info_birthday=?,info_mail=?,info_idnum=? where id=?";
		try {
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1,user.getUsername());
			pstmt.setInt(2,user.getAge());
			pstmt.setString(3, user.getSex());
			pstmt.setString(4, user.getAddress());
			pstmt.setString(5, user.getPhone());
			java.util.Date date = user.getInfo_birthday();
			pstmt.setDate(6, new java.sql.Date(date.getTime()));
			pstmt.setString(7, user.getInfo_mail());
			pstmt.setString(8, user.getInfo_idnum());
			pstmt.setInt(9, user.getId());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			try {
				if(pstmt!=null)
					pstmt.close();
				if(conn!=null)
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	/**
	 * 从数据库中校验密保问题是否正确
	 * @param info_birthday
	 * @param info_mail
	 * @param info_idnum
	 * @return
	 */
	public User updatePassword(Date info_birthday,String info_mail,String info_idnum){
		Connection conn=null;
		conn=DbUtil.getDbutil().getConnection();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		User user=null;
		String sql="select * from dr_user where info_birthday=? and info_mail=? and info_idnum=?";
		try {
			pstmt=conn.prepareStatement(sql);
			java.util.Date date = info_birthday;
			pstmt.setDate(1, new java.sql.Date(date.getTime()));
			pstmt.setString(2, info_mail);
			pstmt.setString(3, info_idnum);
			rs=pstmt.executeQuery();
			if(rs.next()){
				user=new User();
				user = new User();
				user.setId(rs.getInt("id"));
				user.setAge(rs.getInt("age"));
				user.setPower(rs.getInt("power"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				user.setPhone(rs.getString("phone"));
				user.setAddress(rs.getString("address"));
				user.setSex(rs.getString("sex"));
				user.setInfo_birthday(rs.getDate("info_birthday"));
				user.setInfo_mail(rs.getString("info_mail"));
				user.setInfo_idnum(rs.getString("info_idnum"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			try {
				if(pstmt!=null)
					pstmt.close();
				if(conn!=null)
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return user;
	}
	/**
	 * 通过id更新密码
	 * @param user
	 */
	public void password(String password,int id){
		Connection conn=null;
		conn=DbUtil.getDbutil().getConnection();
		PreparedStatement pstmt=null;
		String sql="update dr_user set password=? where id=?";
		try {
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, password);
			pstmt.setInt(2, id);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			try {
				if(pstmt!=null)
					pstmt.close();
				if(conn!=null)
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public User selectone(int id){
		Connection conn=DbUtil.getDbutil().getConnection();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		User user=null;
		String sql="select * from dr_user where id=?";
		try {
			pstmt=conn.prepareStatement(sql);
			pstmt.setInt(1, id);
			rs=pstmt.executeQuery();
			if(rs.next()){
				user=new User();
				user.setUsername(rs.getString("username"));
				user.setAddress(rs.getString("address"));
				user.setPhone(rs.getString("phone"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			try {
				if(rs!=null)
					rs.close();
				if(pstmt!=null)
					pstmt.close();
				if(conn!=null)
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return user;
	}
	public int changeaddress(int id,String address){
		Connection conn=DbUtil.getDbutil().getConnection();
		PreparedStatement pstmt=null;
		int zz = -1;
		String sql="update dr_user set address=? where id=?";
		try {
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, address);
			pstmt.setInt(2, id);
			zz=pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			try {
				if(pstmt!=null)
					pstmt.close();
				if(conn!=null)
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return zz;
	}
	public List AllUser(){
		List<String> list = new ArrayList<String>();
		Connection conn=DbUtil.getDbutil().getConnection();
		PreparedStatement pstmt=null;
		ResultSet rs = null;
		String sql="select * from dr_user";
		try {
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while(rs.next()){
				list.add(rs.getString("username"));
			}
		} catch (SQLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		return list;
	}
}