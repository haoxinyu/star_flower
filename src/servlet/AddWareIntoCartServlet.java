package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.Cart;
import entity.Classs;
import entity.User;
import service.CartService;
import service.ClasssService;

/**
 * Servlet implementation class AddWareIntoCartServlet
 */
@WebServlet("/AddWareIntoCartServlet")
public class AddWareIntoCartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//从前台页面获取用户id，商品id，购买数量
		int userid=Integer.parseInt(request.getParameter("userid"));
		int wareid=Integer.parseInt(request.getParameter("wareid"));
		int warenum=Integer.parseInt(request.getParameter("warenum"));
		
		User user=(User) request.getSession().getAttribute("user");
		
		Cart cart = new Cart();
		cart.setUserid(userid);
		cart.setWareid(wareid);
		cart.setType(0);
		cart.setWarenum(warenum);
		
		
		CartService cartservice=new CartService();
		//根据商品名，用户名，购买数量向购物车表中新增加一条记录
		cartservice.add(cart);
		
		
		List<Cart> cartlist=cartservice.findallcarts(cart);
		double s=0;
		for(int i=0;i<cartlist.size();i++){
			s=s+cartlist.get(i).getWareprice()*cartlist.get(i).getWarenum();
		}
		int cartlistsize=cartlist.size();
		
		for(int i=0;i<cartlistsize;i++){
			System.out.println("购物车内的商品"+cartlist.get(i).getWarenum());
		}
		System.out.println("*****");
		
		request.getSession().setAttribute("user", user);
		request.getSession().setAttribute("cartlist", cartlist);
		request.getSession().setAttribute("s", s);
		request.getSession().setAttribute("cartlistsize", cartlistsize);
		
		User usertext=(User) request.getSession().getAttribute("user");
		System.out.println("给用户"+usertext.getId()+"添加商品");
		
		response.sendRedirect("JumpIntoCart");
		//request.getRequestDispatcher("WEB-INF/page/cart.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
