package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.User;
import service.UserService;

/**
 * Servlet implementation class UpdatePasswordServlet
 */
@WebServlet("/PasswordServlet")
public class PasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int id=(int) request.getSession().getAttribute("id");
		String password=request.getParameter("password");
		String qrpasswordd=request.getParameter("qrpassword");
		User user=new User();
		user.setId(id);
		user.setPassword(password);
		UserService userservice=new UserService();
		userservice.password(password, id);
		if(password.equals(qrpasswordd)){
			user.setPassword(password);
			request.getRequestDispatcher("WEB-INF/page/myaccount.jsp").forward(request, response);
		}
		else{
			request.setAttribute("errorpassword", "两次输入的密码不一致");
			request.getRequestDispatcher("WEB-INF/page/password.jsp").forward(request, response);
		}
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
