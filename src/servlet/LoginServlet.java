package servlet;

import java.io 

.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.Cart;
import entity.User;
import service.CartService;
import service.UserService;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String username=request.getParameter("username");
		username=new String(username.getBytes("iso8859-1"),"utf-8");
		String password=request.getParameter("password");
		UserService userservice=new UserService();
		User user=null;
		user = userservice.login(username, password);
			 
			if(user!=null){
				int id=user.getId();
				
				if(user.getPower()==1){
					CartService cartservice=new CartService();
					Cart cart = new Cart();
					cart.setUserid(user.getId());
					List<Cart> cartlist=cartservice.findallcarts(cart);
					double s=0;
					for(int i=0;i<cartlist.size();i++){
						s=s+cartlist.get(i).getWareprice()*cartlist.get(i).getWarenum();
					}
					int cartlistsize=cartlist.size();
					request.getSession().setAttribute("cartlist", cartlist);
					request.getSession().setAttribute("s", s);
					request.getSession().setAttribute("cartlistsize", cartlistsize);
					request.getSession().setAttribute("user", user);
					request.getSession().setAttribute("name", username);
					request.getSession().setAttribute("id", id);
					request.getRequestDispatcher("index.jsp").forward(request, response);
				}
				else{
					request.getRequestDispatcher("WEB-INF/page/Amain.jsp").forward(request, response);
				}
			}
			else{
				String msg="��¼�����������";
				request.setAttribute("msg", msg);
				request.getSession().setAttribute("user", null);
				request.getRequestDispatcher("WEB-INF/page/myaccount.jsp").forward(request, response);
				
			}
}
		//response.getWriter().append("Served at: ").append(request.getContextPath());


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
