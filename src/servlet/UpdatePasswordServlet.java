package servlet;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.User;
import service.UserService;

/**
 * Servlet implementation class PasswordServlet
 */
@WebServlet("/UpdatePasswordServlet")
public class UpdatePasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if("".equals(request.getParameter("username"))||
				"".equals(request.getParameter("info_birthday"))||
				"".equals(request.getParameter("info_mail"))||
				"".equals(request.getParameter("info_idnum"))){
			request.getRequestDispatcher("LostPasswordServlet").forward(request, response);
		}
		else{
			String username=request.getParameter("username");
			String info_Birthday = request.getParameter("info_birthday");
			//System.out.println(info_Birthday);	
			java.text.SimpleDateFormat formatter = new SimpleDateFormat( "yyyy-MM-dd ");
			info_Birthday+=" ";
			Date date=null;
			try {
				date =  formatter.parse(info_Birthday);
			} catch (ParseException e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			}
			System.out.println(date);
			String info_mail=request.getParameter("info_mail");
			String info_idnum=request.getParameter("info_idnum");
			UserService userservice=new UserService();
			User user=userservice.updatePassword(date, info_mail, info_idnum);
			System.out.println(user);
			if(user!=null){
				String username1=user.getUsername();
				System.out.println(username1);
				System.out.println("$$$$$");
				int id=user.getId();
				System.out.println("需要修改密码的"+id);
				if(username.equals(username1)){
					request.getSession().setAttribute("id", id);
					System.out.println("session中的"+request.getSession().getAttribute("id"));
					request.getRequestDispatcher("WEB-INF/page/password.jsp").forward(request, response);
				}
				else{
					request.setAttribute("error", "信息错误");
					request.getRequestDispatcher("WEB-INF/page/updatepassword.jsp").forward(request, response);
				}
			}
			else{
				request.setAttribute("error", "信息错误");
				request.getRequestDispatcher("WEB-INF/page/updatepassword.jsp").forward(request, response);
			}
		}
		
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
