package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ClasssDao;
import entity.Classs;

/**
 * Servlet implementation class Anewclasss
 */
@WebServlet("/Anewclasss")
public class Anewclasss extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Anewclasss() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ClasssDao cd=new ClasssDao();
		Classs cc=new Classs();
		cc.setClasss_name(request.getParameter("classname"));
		if(request.getParameter("classname")!="") {
			cd.addClasss(cc);
		}
		request.getRequestDispatcher("WEB-INF/page/Aclass.jsp").forward(request, response);
		//response.sendRedirect("Aclass.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
