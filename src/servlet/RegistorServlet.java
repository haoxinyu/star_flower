package servlet;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.User;
import service.UserService;

/**
 * Servlet implementation class RegistorServlet1
 */
@WebServlet("/RegistorServlet")
public class RegistorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegistorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		UserService userservice=new UserService();
		int maxid=userservice.selectMaxId()+1;
		String username=request.getParameter("username");
		username=new String(username.getBytes("iso8859-1"),"utf-8");
		String password=request.getParameter("password");
		int age=Integer.parseInt(request.getParameter("age"));
		String sex1=request.getParameter("sex1");
		String sex2=request.getParameter("sex2");
		String sex=sex1+sex2;
		String address1=request.getParameter("province");
		String address2=request.getParameter("city");
		String address3=request.getParameter("town");
		String address4=request.getParameter("address1");
		String address=address1+address2+address3+address4;
		address=new String(address.getBytes("iso8859-1"),"utf-8");
		String phone=request.getParameter("phone");
		String info_Birthday = request.getParameter("info_birthday");
		//System.out.println(info_Birthday);
		java.text.SimpleDateFormat formatter = new SimpleDateFormat( "yyyy-MM-dd ");
		info_Birthday+=" ";
		Date date=null;
		try {
			date =  formatter.parse(info_Birthday);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		String info_mail=request.getParameter("info_mail");
		String info_idnum=request.getParameter("info_idnum");
		
		User user=new User();
		user.setId(maxid);
		user.setUsername(username);
		user.setPassword(password);
		user.setAge(age);
		user.setSex(sex);
		user.setAddress(address);
		user.setPhone(phone);
		user.setInfo_birthday(date);
		user.setInfo_mail(info_mail);
		user.setInfo_idnum(info_idnum);
		userservice.registor(user);
		//request.getRequestDispatcher("myaccount.jsp").forward(request, response);
		request.getRequestDispatcher("WEB-INF/page/JumpIntoLoginServlet").forward(request, response);
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
