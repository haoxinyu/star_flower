package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.Cart;
import service.CartService;

/**
 * Servlet implementation class DeleteCartServlet
 */
@WebServlet("/DeleteCartServlet")
public class DeleteCartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String serialnum=request.getParameter("serialnum");
		CartService cartservice= new CartService();
		cartservice.deletecart(serialnum);
		Cart cart=new Cart();
		cart.setUserid(Integer.parseInt(request.getParameter("userid")));
		cart.setWareid(Integer.parseInt(request.getParameter("wareid")));
		List<Cart> cartlist=cartservice.findallcarts(cart);
		
		request.getSession().setAttribute("cartlist", cartlist);
		int cartlistsize=cartlist.size();
		request.getSession().setAttribute("cartlistsize", cartlistsize);
		
		double s=0;
		for(int i=0;i<cartlist.size();i++){
			s=s+cartlist.get(i).getWareprice()*cartlist.get(i).getWarenum();
		}
		request.getSession().setAttribute("s", s);
		response.sendRedirect("JumpIntoCart");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
