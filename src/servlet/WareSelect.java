package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.Classs;
import entity.Pager;
import entity.Ware;
import service.ClasssService;
import service.WareService;

/**
 * Servlet implementation class WareSelect
 */
@WebServlet("/WareSelect")
public class WareSelect extends HttpServlet {
private static final long serialVersionUID = 1L;
    

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	//	HttpSession session = request.getSession();
		WareService wareservice = new WareService();
		String classb = null;
//		classb = "�Ǵ���";
		String classs = null;
		classs = request.getParameter("classs");
		List<Ware> list = new ArrayList<Ware>();
		System.out.println(classs);
	/*	if(classb.equals("")&&classs.equals("")){
			list = wareservice.find(classb,classs);
	//		if()
		}else if(classb.equals("")){
			list = wareservice.find(classb,1);
		}else if(classs.equals("")){
			list = wareservice.find(nclassb,2);
		}*/
		if(classb!=null&&classs==null){
			list = wareservice.find(classs,1);
		}else if(classs!=null&&classb==null){
			list = wareservice.find(classs,2);
		}else if(classb==null&&classs==null) {
			list = wareservice.all();
		}
	//	session.setAttribute("ware", list);*/
		Pager<Ware> pager=new Pager<Ware>();
		pager.setPageUrl("WareSelect");
		pager.setPageNumber(1);
		pager.setRowsPerPage(99);
		pager.setTotalRecords(list.size());
		pager.init();
		pager.setResultList(list);
		
		ClasssService classsService= new ClasssService();
		List<Classs> classsList=classsService.findAll();
		request.setAttribute("classsList", classsList);
		request.setAttribute("page", pager);
		request.getRequestDispatcher("WEB-INF/page/category.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
	}

}
