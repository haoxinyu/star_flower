package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.Cart;

/**
 * Servlet implementation class FindselectnumServlet
 */
@WebServlet("/FindSelectCartServlet")
public class FindSelectCartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String list=(String) request.getParameter("list");
		ArrayList<Cart> u=(ArrayList<Cart>) request.getSession().getAttribute("cartlist");
		String [] t=list.split(",");
		ArrayList<Integer> selectlist=new ArrayList<Integer>();
		double sprice=0;
		for(int i=0;i<t.length;i++)
		{
			selectlist.add(Integer.parseInt(t[i]));
			int temp=selectlist.get(i);
			sprice=sprice+u.get(temp).getWarenum()*u.get(temp).getWareprice();
		}
		int selectlength=selectlist.size();
		
		request.getSession().setAttribute("selectlength", selectlength);
		request.getSession().setAttribute("selectlist", selectlist);
		request.getSession().setAttribute("sprice", sprice);
		
		response.setContentType("text/html");
		PrintWriter p=response.getWriter();
		String result=sprice+","+selectlength;
		request.getSession().setAttribute("temp", 2);
		p.write(result);
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
