package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.CartDao;
import entity.Classs;
import entity.User;
import entity.Ware;
import service.CartService;
import service.ClasssService;
import service.WareService;

/**
 * Servlet implementation class WareServlet
 */
@WebServlet("/WareServlet")
public class WareServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("固定图片的id="+id);
		WareService wareservice = new WareService ();
		Ware ware = null;
		ware = wareservice.query(id);
		System.out.println("商品名字"+ware.getName());
		String zhekou=ware.getClassb();
		if(zhekou.equals("无折扣")){
			
		}
		else{
		double p=(Double.parseDouble(zhekou.charAt(0)+"")*10+Double.parseDouble(zhekou.charAt(1)+""))/100;
			System.out.println(p);
			ware.setPrice((int) (ware.getPrice()*p));
		}
		CartService cartService = new CartService();
		int val=cartService.SelectPCount(ware.getId());
		if(request.getSession().getAttribute("user")==null){
			request.getRequestDispatcher("JumpIntoLoginServlet").forward(request, response);
		}
		else{
		request.getSession().setAttribute("val", val);
		request.setAttribute("ware",ware);
		String inf = ware.getInfo();
		String info = inf.substring(0,inf.indexOf("。",inf.indexOf("。")+1)+1);
		List<Ware> warelist=wareservice.all();
		request.setAttribute("warelist", warelist);
		request.setAttribute("inf", inf);
		request.setAttribute("info", info);
		ClasssService classsService= new ClasssService();
		List<Classs> classsList=classsService.findAll();
		request.setAttribute("classsList", classsList);
		request.getRequestDispatcher("WEB-INF/page/details.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
