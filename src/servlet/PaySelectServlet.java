package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.Cart;
import service.CartService;

/**
 * Servlet implementation class PaySelectServlet
 */
@WebServlet("/PaySelectServlet")
public class PaySelectServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList<Cart> cartlist=(ArrayList<Cart>) request.getSession().getAttribute("cartlist");
		ArrayList<Integer> selectlist=(ArrayList<Integer>) request.getSession().getAttribute("selectlist");
		
		int selectlength=(int)request.getSession().getAttribute("selectlength");
		
		CartService cartservice=new CartService();
		for(int i=0;i<selectlength;i++){
			System.out.println("进入循环");
			cartservice.changecarttype(cartlist.get(selectlist.get(i)).getSerialnum());
			cartlist.get(selectlist.get(i)).setType(1);
		}
		request.getSession().setAttribute("cartlist", cartlist);
/*		System.out.println("能够结束");*/
		request.getRequestDispatcher("WEB-INF/page/contact.jsp").forward(request, response);
		/*response.sendRedirect("contact.jsp");*/
		
//		System.out.println(selectlength+","+sprice);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
