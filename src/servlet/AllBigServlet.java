package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.Classs;
import entity.Ware;
import service.ClasssService;
import service.WareService;

/**
 * Servlet implementation class AllBigServlet
 */
@WebServlet("/AllBigServlet")
public class AllBigServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		WareService wareservice = new WareService();
		String classb = "���ۿ�";
	//	classs = request.getParameter("classs");
		List<Ware> list = new ArrayList<Ware>();
		list = wareservice.find(classb, 1);
		request.setAttribute("cuxiao", list);
		ClasssService classsService= new ClasssService();
		List<Classs> classsList=classsService.findAll();
		request.setAttribute("classsList", classsList);
		request.getRequestDispatcher("WEB-INF/page/specials.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
