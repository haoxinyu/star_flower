package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import entity.User;
import service.UserService;

/**
 * Servlet implementation class QuaryInfoServlet
 */
@WebServlet("/QuaryInfoServlet")
public class QuaryInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int id=Integer.parseInt(request.getParameter("id"));
		HttpSession session = request.getSession();
		UserService userservice=new UserService();
		User user=userservice.quaryInfo(id);
		session.setAttribute("user", user);
	//	session.getRequestDispatcher("page/updateInfo.jsp").forward(request, response);
		request.getRequestDispatcher("WEB-INF/page/updateInfo.jsp").forward(request, response);
		/*response.sendRedirect("updateInfo.jsp");*/
	//	response.sendRedirect("page/updateInfo.jsp");
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
