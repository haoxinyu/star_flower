package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.Cart;
import entity.Classs;
import service.CartService;
import service.ClasssService;


/**
 * Servlet implementation class COrderServlet
 */
@WebServlet("/COrderServlet")
public class COrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		//response.getWriter().append("Served at: ").append(request.getContextPath());
		if(request.getParameter("serialnum")==null){
			System.out.println("订单号不能为空");
			request.getRequestDispatcher("order.jsp").forward(request, response);//转发，页面的跳转//			
			return;
		}
		
		String serialnum=request.getParameter("serialnum");
		
		CartService cartservice=new CartService();
		Cart cart=cartservice.CSelectOrder(serialnum);
		
		if(cart!=null){
			request.setAttribute("cart", cart);
			ClasssService classsService= new ClasssService();
			List<Classs> classsList=classsService.findAll();
			request.setAttribute("classsList", classsList);
			System.out.println("我是servlet中的"+cart.getSerialnum());
			request.getRequestDispatcher("WEB-INF/page/CSelectOrder.jsp").forward(request, response);
			}
		else{
			
			request.getRequestDispatcher("WEB-INF/page/order.jsp").forward(request, response);
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}