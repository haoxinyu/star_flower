package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.Classs;
import entity.Pager;
import entity.Ware;
import service.ClasssService;
import service.WareService;

/**
 * Servlet implementation class AllSelect
 */
@WebServlet("/AllSelect")
public class AllSelect extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int pageNum;
		if (null == request.getParameter("pageNum")) {
			pageNum = 1;
		} else {
			pageNum = (Integer.parseInt(request.getParameter("pageNum")));
		}

		WareService wareservice = new WareService();
		List<Ware> list = wareservice.queryAllWareByPage(pageNum, 9);
		Pager<Ware> pager = new Pager<Ware>();

		int nums = wareservice.all().size();
		int numss;
		if (nums % 9 == 0) {
			numss = nums / 9;
		} else {
			numss = nums / 9 + 1;
		}

		pager.setPageUrl("AllSelect");

		pager.setPageNumber(pageNum);
		// pager.setTotalPages(numss);
		pager.setTotalRecords(nums);
		pager.init();
		pager.setResultList(list);
		ClasssService classsService = new ClasssService();
		List<Classs> classsList = classsService.findAll();
		request.setAttribute("classsList", classsList);
		request.setAttribute("page", pager);
		// System.out.println(pager.getPageLink());
		request.getRequestDispatcher("WEB-INF/page/category.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
