package servlet;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import entity.Ware;
import service.WareService;

/**
 * Servlet implementation class Achangeware
 */
@WebServlet("/Achangeware")
public class Achangeware extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Achangeware() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Ware ww=new Ware();
		WareService ws=new WareService();
		response.setCharacterEncoding("utf-8");
		boolean ismutipart =ServletFileUpload.isMultipartContent(request);
		if(ismutipart){
			DiskFileItemFactory factory=new DiskFileItemFactory();
			ServletFileUpload upload=new ServletFileUpload(factory);
			upload.setHeaderEncoding("utf-8");
			List<FileItem> list;
			try {
				list=upload.parseRequest(request);
				int i=0;
				for(FileItem fileitem:list){
					if(fileitem.isFormField()){
						String name = fileitem.getFieldName(); 
						String value = fileitem.getString(); 
						if(name.equals("id")){
							ww.setId(Integer.parseInt(value));	
//							System.out.println(ww.getId());
						}
						if(name.equals("name")){
							ww.setName(new String(value.getBytes("iso8859-1"),"utf-8"));	
						}
						if(name.equals("classb")){
							ww.setClassb(new String(value.getBytes("iso8859-1"),"utf-8"));
							//System.out.println(ww.getClassb());
						}
						if(name.equals("classs")){
							ww.setClasss(new String(value.getBytes("iso8859-1"),"utf-8"));
							//System.out.println(ww.getClassb());
						}
						if(name.equals("num")){
							ww.setNum(Integer.parseInt(value));
							//System.out.println(ww.getClassb());
						}
						if(name.equals("price")){
							ww.setPrice(Integer.parseInt(value));
							//System.out.println(ww.getClassb());
						}
						if(name.equals("info")){
							ww.setInfo(new String(value.getBytes("iso8859-1"),"utf-8"));
							System.out.println(ww.getInfo());
						}
					}
					else{
						String name=fileitem.getName();
					//	String  value = fileitem.getString();
						if(name.isEmpty()){
						}
						else{
						System.out.println("name:"+name);
						//System.out.println("value:"+value);
						i++;
						String newname = null;
						if(i==1){
							newname=ww.getId()+"photo1"+name.substring(name.indexOf('.'));
							ww.setPicture1(newname);
						}
						if(i==2){
							newname=ww.getId()+"photo2"+name.substring(name.indexOf('.'));
							ww.setPicture2(newname);
						}
						if(i==3){
							newname=ww.getId()+"photo3"+name.substring(name.indexOf('.'));
							ww.setPicture3(newname);
						}
						File file=new File(request.getSession().getServletContext().getRealPath("")+"photo\\",newname);
						fileitem.write(file);
						}
					}
				}
			} catch ( Exception e) {
				e.printStackTrace();
			}
			
		}
		int pp=ws.update(ww);
		request.getRequestDispatcher("WEB-INF/page/Aware.jsp").forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
